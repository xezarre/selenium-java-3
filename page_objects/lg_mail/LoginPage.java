package lg_mail;

import elements.CustomElement;
import lg_email.EmailAccount;

public class LoginPage extends GeneralPage{
	
	private CustomElement liteVersionChbx = new CustomElement("//*[@for='chkBsc']");
	private CustomElement usernameTxt = new CustomElement("//*[@id='username']");
	private CustomElement passwordTxt = new CustomElement("//*[@id='password']");
	private CustomElement submitBtn = new CustomElement("//input[@type='submit']");
	
	public MainPage login(EmailAccount account) {
		
		usernameTxt.enter(account.getUsername());
		passwordTxt.enter(account.getPassword());
		submitBtn.click();
		return new MainPage();
	}
	
	public MainPage loginLiteVersion(EmailAccount account) {
		
		liteVersion = true;
		liteVersionChbx.click();
		return login(account);
	}

}
