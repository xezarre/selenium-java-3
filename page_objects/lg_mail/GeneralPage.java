package lg_mail;

import java.util.ArrayList;

import driver.DriverManager;
import utils.Logger;

public class GeneralPage {

	Logger log = new Logger();
	static Boolean liteVersion = false;
	
	String currentPage = DriverManager.getDriver().getWindowHandle();
	ArrayList<String> handler = DriverManager.getDriver().getWindowHandles();
	
	public void closeTab() {
		DriverManager.getDriver().close();
		DriverManager.getDriver().switchTo(currentPage);
	}
}
