package lg_mail;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import driver.DriverManager;
import elements.CustomElement;
import lg_email.Attachment;
import lg_email.EmailMessage;
import utils.Utilities;

public class NewMessagePage extends GeneralPage {
	
	private CustomElement toTxt = new CustomElement("//*[@id='txtto' or @id='divTo']");
	private CustomElement ccTxt = new CustomElement("//*[@id='txtcc' or @id='divCc']");
	private CustomElement bccTxt = new CustomElement("//*[@id='txtbcc']");
	private CustomElement sbjTxt = new CustomElement("//*[@id='txtsbj' or @id='txtSubj']");
	private CustomElement contentTxt = new CustomElement("//body[@ocsi]");
	private CustomElement contentTxt_lite = new CustomElement("//*[@name='txtbdy']");
	
	private CustomElement sendBtn = new CustomElement("//*[@id='lnkHdrsend' or @id='divToolbarButtonsend']");
	private CustomElement attachFileBtn = new CustomElement("//*[@id='divToolbarButtonattachfile' or @id='lnkHdrattachfile']");
	private CustomElement insertImageBtn = new CustomElement("//*[@id='divToolbarButtoninsertimage']");
	private CustomElement saveBtn = new CustomElement("//*[@id='lnkHdrsave' or @id='divToolbarButtonsave']");
	private CustomElement closeBtn = new CustomElement("//*[@id='lnkHdrclose']"); //lite
	
	private CustomElement attachmentFrm = new CustomElement("//body[@class='wh100']/iframe");
	private CustomElement chooseFileBtn1 = new CustomElement("//*[@id='divFile1']");
	private CustomElement attachBtn = new CustomElement("//button[@id='btnAttch']");
	
	
	public NewMessagePage() {
		if (!liteVersion.equals(true))
			DriverManager.getDriver().switchTo(handler.get(1));
	}
	
	public NewMessagePage composeWithAttachment(EmailMessage message) {
		
		if (!liteVersion.equals(true))
			return this.compose(message).attach(message.getDocumentAttachments());
		else 
			return this.compose(message).openAttachDocumentPage().attachFile(message.getDocumentAttachments()); // lite
	}
	
	public NewMessagePage composeWithPicture(EmailMessage message) {
		
		if (!liteVersion.equals(true))
			return this.compose(message).insertImage(message.getImageAttachments());
		else
			return this.compose(message).openAttachDocumentPage().attachFile(message.getImageAttachments()); // lite
	}
	
	public NewMessagePage compose(EmailMessage message) {
		
		toTxt.type(message.getReceiver());
		ccTxt.type(message.getCCReceiver());
		
		if (liteVersion.equals(true))
			bccTxt.enter(message.getBCCReceiver());
		
		sbjTxt.enter(message.getSubject());
		
		if (liteVersion.equals(false)) {
			DriverManager.getDriver().switchFrame("ifBdy");
			contentTxt.type(message.getBody());
			DriverManager.getDriver().switchFrame("default");;
		} else {
			contentTxt_lite.type(message.getBody());
		}
		
		return this;
	}
	
	private NewMessagePage insertImage(ArrayList<Attachment> attachments) {

		// Click Insert Image button
		insertImageBtn.click();
		
		// Switch to Insert Inline Picture iFrame
		DriverManager.getDriver().switchFrame("iFrameModalDlg");
		attachmentFrm.waitForVisibility(1);
		DriverManager.getDriver().switchFrame(0);
		
		attachments.forEach((attachment) -> {
			
			chooseFileBtn1.click();
			String absoluteFilePath = Utilities.getProjectPath() + attachment.getFilePath() + attachment.getFileName();
			selectFileToAttach(absoluteFilePath);
		});
		
		attachBtn.click();
		DriverManager.getDriver().switchFrame("default");
		
		return this;
	}

	public NewMessagePage attach(ArrayList<Attachment> attachments) {
		
		// Click Attach File button
		attachFileBtn.click();
		
		// Switch to Attach iFrame
		DriverManager.getDriver().switchFrame("iFrameModalDlg");
		attachmentFrm.waitForVisibility(1);
		DriverManager.getDriver().switchFrame(0);
		
		attachments.forEach((attachment) -> {
			
			chooseFileBtn1.click();
			String absoluteFilePath = Utilities.getProjectPath() + attachment.getFilePath() + attachment.getFileName();
			selectFileToAttach(absoluteFilePath);
		});
		
		attachBtn.click();
		DriverManager.getDriver().switchFrame("default");
		
		return this;
	}
	
	private void selectFileToAttach(String filePath) {
		
		StringSelection clipboardString = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipboardString, null);
		
		Robot robot;
		try {
			robot = new Robot();
			
			robot.delay(1000);
			robot.keyPress(KeyEvent.VK_CONTROL);		 
			robot.keyPress(KeyEvent.VK_V);		 
			robot.keyRelease(KeyEvent.VK_V);		 
			robot.keyRelease(KeyEvent.VK_CONTROL);		 
			robot.delay(500);
			//System.out.println("Paste");
			
			robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            //System.out.println("Enter");

            robot.delay(500);
        } catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	public MainPage save() {
		
		if (!liteVersion.equals(true)) {
			saveBtn.click();
			DriverManager.getDriver().close();
		} else {
			saveBtn.click();
			closeBtn.click();
		}
		return new MainPage();
	}
	
	public MainPage send() {
		sendBtn.click();
		return new MainPage();
	}

	private AttachDocumentPage openAttachDocumentPage() {
		
		attachFileBtn.click();
		return new AttachDocumentPage();
	}

}
