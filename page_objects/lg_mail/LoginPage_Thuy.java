package lg_mail;

import elements.CustomElement;

public class LoginPage_Thuy {

	// Locators section

	private CustomElement txtUserName = new CustomElement("//input[@id='username']");
	private CustomElement txtPassword = new CustomElement("//input[@id='password']");
	private CustomElement btnLogin = new CustomElement("//input[@type='submit']");
			
	// Elements section

	// Methods Section
	/**
	 * @author Thuy Tran
	 * @param username
	 * @param password
	 */
	public void login(String username, String password) {
		txtUserName.waitForClickable(5);
		txtUserName.enter(username);
		txtPassword.waitForClickable(5);
		txtPassword.enter(password);
		btnLogin.click();
	}
}
