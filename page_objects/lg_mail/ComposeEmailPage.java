package lg_mail;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import driver.DriverManager;
import elements.CustomElement;
import lg_email.Email;
import utils.Logger;
import utils.Utilities;

public class ComposeEmailPage extends GeneralPage_Thuy{
	protected WebDriver _webDriver;
	// Locators section

	private CustomElement txtToEmail = new CustomElement("//div[@id='divTo']");
	private CustomElement txtCcEmail = new CustomElement("//div[@id='divCc']");
	private CustomElement txtSubject = new CustomElement("//input[@id='txtSubj']");
	private CustomElement txtContent = new CustomElement("//body[@ocsi]");
	//private By chooseFileInput = By.xpath("//input[@id='attach']");
	
	//private CustomElement ifrContent = new CustomElement("//div[@id='divBdy']/iframe");

	//private CustomElement ifrAttachment = new CustomElement("//iframe[@id='iFrameModalDlg']");
	//private CustomElement ifrAttachment = new CustomElement("//body[@class='wh100']/iframe");
	
	private CustomElement btnSave = new CustomElement("//div[@id='divToolbarButtonsave']");
	private CustomElement btnSend = new CustomElement("//div[@id='divToolbarButtonsend']");	
	private CustomElement btnChooseFile1 = new CustomElement("//div[@id='divFile1']");
	
	private CustomElement btnAttachFile = new CustomElement("//button[@id='btnAttch']");
	//private CustomElement btnHelp = new CustomElement("//div[@class='divDlgAttach']//a[@id='help']");
	
	
	private CustomElement icoAttachFile = new CustomElement("//div[@id='divToolbarButtonattachfile']");
	private CustomElement icoAttachImage = new CustomElement("//div[@id='divToolbarButtoninsertimage']");
	
	//private CustomElement lblAttachDlg = new CustomElement("//span[text()='Include Attachments']");
	
	// Elements section

	// Methods Section
	/**
	 * @author Thuy Tran
	 * @description Compose new email
	 */
	public void composeEmail(Email email) 
	{
		DriverManager.getDriver().switchWindow("Untitled Message");
		
		txtToEmail.waitForClickable();
		txtToEmail.enter(email.getToEmail());
		
		if (email.getCCEmail() != "")
		{
			txtCcEmail.waitForClickable();
			txtCcEmail.enter(email.getCCEmail());
		}
		
		txtSubject.waitForClickable();
		txtSubject.enter(email.getEmailSubject());
		
		DriverManager.getDriver().switchFrame("ifBdy");
		txtContent.waitForClickable();
		txtContent.enter(email.getEmailContent());
		
		DriverManager.getDriver().switchFrame("default");
		
		if (email.getFile() != "")
		{
			attach("File", email.getFile());
		}
		if (email.getPicture() != "")
		{
			attach("Image", email.getPicture());
		}
		
	}
	
	/**
	 * @author Thuy Tran
	 * @description Attache File
	 */

	public void attachFile(String fileName) 
	{ 
		icoAttachFile.click();
		DriverManager.getDriver().switchParentFrame();
		DriverManager.getDriver().switchFrame("iFrameModalDlg");
		DriverManager.getDriver().switchFrame(0);
		
		String attachFile = Utilities.getProjectPath() + "/docs/" + fileName;

		btnChooseFile1.click();
		StringSelection stringSelection= new StringSelection(attachFile);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		
		Robot robot;

		try {
			robot = new Robot();
			// Cmd + Tab is needed since it launches a Java app and the browser looses focus	 
			robot.keyPress(KeyEvent.VK_META);		 
			robot.keyPress(KeyEvent.VK_TAB);		 
			robot.keyRelease(KeyEvent.VK_META);		 
			robot.keyRelease(KeyEvent.VK_TAB);		 
			robot.delay(500);
			// “Go To Folder” on Mac - Hit Command+Shift+G on a Finder window.
			robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_META);

            // Paste the clipBoard content - Command ⌘ + V.
            robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_META);

            // Press Enter (GO - To bring up the file.)
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.delay( 1000 * 4 );

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		btnAttachFile.click();		 	 
	}
	
	/**
	 * @author Thuy Tran
	 * @description Attache File
	 */

	public void attachImage(String image) 
	{ 
		CustomElement btnChooseFile = new CustomElement("(//input[@type='file'])[1]");
		
		Logger logger = new Logger();
		icoAttachImage.waitForClickable();
		icoAttachImage.click();
		DriverManager.getDriver().switchParentFrame();
		DriverManager.getDriver().switchFrame("iFrameModalDlg");
		DriverManager.getDriver().switchFrame(0);
		
		Boolean isbtnChooseFileExisted = btnChooseFile.isExisted();
		logger.printMessage("is btnChooseFile existed: " + isbtnChooseFileExisted);
		
		Boolean isFileExisted = new File("/Users/thuytran/selenium-java-3/docs/Test_Image.png").exists();
		logger.printMessage("is isFileExisted existed: " + isFileExisted);

		btnChooseFile1.click();
		String attachFile = Utilities.getProjectPath() + "/docs/" + image;
		StringSelection stringSelection= new StringSelection(attachFile);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		
		Robot robot;

		try {
			robot = new Robot();
			// Cmd + Tab is needed since it launches a Java app and the browser looses focus	 
			robot.keyPress(KeyEvent.VK_META);		 
			robot.keyPress(KeyEvent.VK_TAB);		 
			robot.keyRelease(KeyEvent.VK_META);		 
			robot.keyRelease(KeyEvent.VK_TAB);		 
			robot.delay(500);
			// “Go To Folder” on Mac - Hit Command+Shift+G on a Finder window.
			robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_META);

            // Paste the clipBoard content - Command ⌘ + V.
            robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_META);

            // Press Enter (GO - To bring up the file.)
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.delay( 1000 * 4 );

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		btnAttachFile.click();		 	 
	}
	/**
	 * @author Thuy Tran
	 * @description Save the email
	 */
	public void saveEmail() 
	{
		btnSave.click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Send the email
	 */
	public void sendEmail() 
	{
		btnSend.click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Close the compose email window
	 */
	public void closeComposeEmailWindow(String title) 
	{
		DriverManager.getDriver().switchWindow(title);
		DriverManager.getDriver().closeWindow();
		
	}
	
	/**
	 * @author Thuy Tran
	 * @description Attache File or Image
	 */

	public void attach(String type, String fileName) 
	{ 
		if (type == "File")
		{
			icoAttachFile.click();
		}
		else if (type == "Image")
		{
			icoAttachImage.waitForClickable();
			icoAttachImage.click();
		}
		
		DriverManager.getDriver().switchParentFrame();
		DriverManager.getDriver().switchFrame("iFrameModalDlg");
		DriverManager.getDriver().switchFrame(0);
		
		String attachFile = Utilities.getProjectPath() + "/test_data/lg_mail/" + fileName;

		StringSelection stringSelection= new StringSelection(attachFile);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		
		DriverManager.getDriver().getWebDriver().findElement(By.xpath("//input[@id='file1']")).sendKeys(attachFile);
		btnAttachFile.click();		 	 
	}
	
}