package lg_mail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;

import elements.CustomElement;
import lg_email.Email;
import utils.Logger;
import utils.Utilities;

public class GeneralPage_Thuy{
	protected WebDriver _webDriver;
	// Locators section

	private CustomElement btnNewEmail = new CustomElement("//div[@id='divToolbarButtonnewmsgc']");
	private CustomElement lnkDraft = new CustomElement("//span[@id='spnFldrNm' and @fldrnm='Drafts']");
	private CustomElement lnkInbox = new CustomElement("//div[@id='mailtree']//span[@id='spnFldrNm' and @fldrnm='Inbox']");
	
	///////////Email Detail
	private CustomElement lblSubjectDetail = new CustomElement("//div[@url='undefined']//div[@id='divConvTopic']");
	private CustomElement lblToEmailDetail = new CustomElement("//div[@url='undefined']//div[@id='divTo']/span");
	private CustomElement lblContentDetail = new CustomElement("//div[@url='undefined']//div[@id='divBdy']//span");
	private CustomElement lblAttachmentDetail= new CustomElement("//div[@url='undefined']//div[@id='divAtch']//span");
	
	private CustomElement imgContent = new CustomElement("//div[@id='divBdy']//img");
	
	
	
	// Dynamic XPath
	private final String xpathLnkSubject = "//div[@id='divSubject' and text()='%s']";
	private final String xpathLblSubjectDetail = "//div[@id='divConvTopic' and text()='%']";

	
	// Methods Section
	
	/**
	 * @author Thuy Tran
	 * @return The xpath based on the inputed subject value
	 */
	protected CustomElement getLnkEmailSubject(String subject) {
		return new CustomElement(String.format(xpathLnkSubject, subject));
	}
	
	/**
	 * @author Thuy Tran
	 * @return The xpath based on the subject value in the detail email
	 */
	protected CustomElement getLblEmailSubjectDetail(String subject) {
		return new CustomElement(String.format(xpathLblSubjectDetail, subject));
	}
	
	/**
	 * @author Thuy Tran
	 * @description Open Compose Email window
	 */
	public void openNewEmailWindow() {
		Logger logger = new Logger();
		btnNewEmail.waitForClickable();
		logger.printMessage("btnNewEmail: " + btnNewEmail.isExisted());
		btnNewEmail.click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Open Draft page
	 */
	public void openDraftPage() {
		lnkDraft.waitForClickable();
		lnkDraft.click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Open Inbox page
	 */
	public void openInboxPage() {
		lnkInbox.waitForClickable();
		lnkInbox.click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Select email by clicking Email Subject
	 */
	public void selectEmail(String subject) {
		getLnkEmailSubject(subject).waitForClickable();
		getLnkEmailSubject(subject).click();
	}
	
	/**
	 * @author Thuy Tran
	 * @description Get detail info of draft email 
	 */
	/*
	public ArrayList<String> getEmailDetailInfo1() {
		ArrayList<String> emailInfo = new ArrayList<String>();
		lblToEmailDetail.waitForClickable();
		String subject = lblSubjectDetail.getText().toString();
		String toEmail = lblToEmailDetail.getText().toString();
		String content = lblContentDetail.getText().toString();
		String attachment = lblAttachmentDetail.getAttribute("_attname");
		emailInfo.add(subject);
		emailInfo.add(toEmail);
		emailInfo.add(content);
		emailInfo.add(attachment);
		Logger logger = new Logger();
		logger.printMessage("emailInfo:" + emailInfo);
		return emailInfo;
	}
	*/
	public Email getEmailDetailInfo() {
		Email email = new Email();
		lblToEmailDetail.waitForClickable();
		String subject = lblSubjectDetail.getText().toString();
		String toEmail = lblToEmailDetail.getText().toString();
		String content = lblContentDetail.getText().toString();
		String attachment = lblAttachmentDetail.getAttribute("_attname");
		email.setToEmail(toEmail);
		email.setEmailSubject(subject);
		email.setEmailContent(content);
		email.setFile(attachment);
		Logger logger = new Logger();
		logger.printMessage("emailInfo:" + email.getEmailSubject());
		logger.printMessage("emailInfo:" + email.getEmailContent());
		logger.printMessage("emailInfo:" + email.getToEmail());
		logger.printMessage("emailInfo:" + email.getFile());
		return email;
	}
	
	/**
	 * @author Thuy Tran
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @description Download Image
	 */
	public void downloadImage(String imageName) throws MalformedURLException, IOException, IOException {
		String downloadLocation = Utilities.getProjectPath() + "/docs/";
		String src = imgContent.getAttribute("src");
		Logger logger = new Logger();
		logger.printMessage("src:" + src);
		URL url = new URL(src);
		
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(downloadLocation + imageName);
	    byte[] b = new byte[4096];
	    int length;

	    while ((length = is.read(b)) != -1) {
	        os.write(b, 0, length);
	    }

	    is.close();
	    os.close();
	   
		/*
		logger.printMessage("bufferedImage:   " + bufferedImage);
		//RenderedImage renderedImage = ImageIO.read(new URL(src));
		File outputfile = new File("/Users/thuytran/selenium-java-3/docs/saved.png");
		ImageIO.write(bufferedImage, "png", outputfile);
		*/
		//ImageTypeSpecifier.createFromRenderedImage(bufferedImage);
		//ImageTypeSpecifier spec = ImageTypeSpecifier.createFromRenderedImage(bufferedImage);
		//ImageIO.getImageWriters(spec, "jpeg");
	}
	
	/**
	 * @author Thuy Tran
	 * @description get file in folder
	 */
	public Boolean isFileInFolder(String file) {
		
		File folder = new File (Utilities.getProjectPath() + "/docs/");
		 
		//List the files on that folder
		File[] listOfFiles = folder.listFiles();
		boolean found = false;
		
		// Look for the file in the files
		// You should write smart REGEX according to the filename
		for (File listOfFile : listOfFiles) {
			if (listOfFile.isFile()) {
		    	String fileName = listOfFile.getName();
		        System.out.println("File " + listOfFile.getName());
		        if (fileName.matches(file)) {
		        	// File f = new File(fileName);
		        	return true;
		        }
			}
		}
		return found;
	}
	
}
