package lg_mail;

import java.util.ArrayList;

import org.openqa.selenium.By;

import driver.DriverManager;
import elements.CustomElement;
import lg_email.Attachment;
import utils.Utilities;

public class AttachDocumentPage extends NewMessagePage {
	
	private By chooseFileInput = By.xpath("//input[@id='attach']");
	
	private CustomElement attachBtn = new CustomElement("//*[@id='attachbtn']");
	private CustomElement doneBtn = new CustomElement("//*[@id='lnkHdrdone']");
	//private CustomElement removeBtn = new CustomElement("//*[@id='lnkHdrremove']");
	
	public AttachDocumentPage() {}
	
	public NewMessagePage attachFile(ArrayList<Attachment> attachments) {
	
		attachments.forEach((attachment) -> {
			
			String absoluteFilePath = Utilities.getProjectPath() + attachment.getFilePath() + attachment.getFileName();
			DriverManager.getDriver().getWebDriver().findElement(chooseFileInput).sendKeys(absoluteFilePath);
			attachBtn.click();
		});
		doneBtn.click();
		return new NewMessagePage();
	}
	
	

}
