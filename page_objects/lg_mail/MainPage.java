package lg_mail;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Optional;

import driver.DriverManager;
import elements.CustomElement;
import lg_email.Attachment;
import lg_email.EmailMessage;
import utils.Utilities;

public class MainPage extends GeneralPage {
	
	private String emailBySubject = "//div[@id='divSubject' and text()='%s']";
	private String emailBySubject_lite = "//a[text()='%s']";
	
	private CustomElement inboxLnk = new CustomElement("//*[@id='spnFldrNm' and @fldrnm='Inbox' or @title='Inbox']");
	private CustomElement draftsLnk = new CustomElement("//*[@id='spnFldrNm' and @fldrnm='Drafts']");
	private CustomElement newMessageBtn = new CustomElement("//*[@id='lnkHdrnewmsg' or @id='newmsgc']");
	private CustomElement deleteMessageBtn = new CustomElement("//*[@id='delete']");
	
	private String subjectLbl 	= "//div[%s]//div[@id='divConvTopic']";
	private String recipientLbl = "//div[%s]//div[@id='divTo']/span";
	private String bodyLbl 		= "//div[%s]//div[@id='divBdy']//span";
	private String attachmentNameLbl = "//div[%s]//div[@id='divAtch']//span";
	private String attachedImg = "//div[@id='divBdy']//img";
	
	private String attachedImg_lite = "//*[@id='lnkAtmt']";
	
	private String subjectLbl_lite = "//table[@class='msgHd']//tr[1]/td";
	private String bodyLbl_lite = "//*[@class='PlainText']";
	
	private final String draftMail = "@url='undefined'";
	private final String newMail = "@id='ifRP'";
	
	public MainPage() {
		if (!liteVersion.equals(true))
			DriverManager.getDriver().switchTo(handler.get(0));
	}
	
	public NewMessagePage createNewMessage() {
		newMessageBtn.click();
		return new NewMessagePage();
	}
	
	public MainPage openInboxFolder() {
		inboxLnk.click();
		return this;
	}
	
	public EmailMessage selectNewMail(String subject) {
		
		if (subject.trim().isEmpty())
			subject = "(no subject)";
		
		//CustomElement ce = new CustomElement(String.format(emailBySubject, subject));
		CustomElement ce;
		
		if (liteVersion.equals(true))
			ce = new CustomElement(String.format(emailBySubject_lite, subject));
		else 
			ce = new CustomElement(String.format(emailBySubject, subject));
		
		while (!ce.isDisplayed()) {
			DriverManager.getDriver().refresh(); // To wait for email 
		}
		
		ce.click();
		
		EmailMessage returnMessage = new EmailMessage();
		
		if (liteVersion.equals(true)) {
			returnMessage.setSubject(new CustomElement(subjectLbl_lite).getText());
			returnMessage.setReceiver("vinh.tuan.nguyen@logigear.com");
			returnMessage.setBody(new CustomElement(bodyLbl_lite).getText());
		}
		else {
			returnMessage.setSubject(new CustomElement(String.format(subjectLbl, newMail)).getText());
			returnMessage.setReceiver(new CustomElement(String.format(recipientLbl, newMail)).getAttribute("title"));
			returnMessage.setBody(new CustomElement(String.format(bodyLbl, newMail)).getText());
		}
		
		return returnMessage;
	}
	
	public MainPage openDraftFolder() {
		draftsLnk.click();
		return this;
	}
	
	public EmailMessage selectDraftMail(String subject) {
		
		if (subject.trim().isEmpty())
			subject = "(no subject)";

		new CustomElement(String.format(emailBySubject, subject)).click();
		
		EmailMessage returnMessage = new EmailMessage();
		
		returnMessage.setSubject(new CustomElement(String.format(subjectLbl, draftMail)).getText());
		returnMessage.setReceiver(new CustomElement(String.format(recipientLbl, draftMail)).getAttribute("title"));
		returnMessage.setBody(new CustomElement(String.format(bodyLbl, draftMail)).getText());
		
		Attachment attachment = new Attachment(new CustomElement(String.format(attachmentNameLbl, draftMail)).getAttribute("_attname"));
		returnMessage.setAttachment(new ArrayList<Attachment>( Arrays.asList(attachment) ));
		
		return returnMessage;
	}
	
	public File downloadImage() {
		
		WebDriver driver = DriverManager.getDriver().getWebDriver();
		
		Actions action = new Actions(driver); 
		
		if (liteVersion.equals(true)) {
			new CustomElement(attachedImg_lite).click(); // lite version
		} else {
			action.contextClick(driver.findElement(By.xpath(attachedImg))).build().perform(); // Right click against picture.
		}
		
		String filePath = Utilities.getProjectPath() + "\\test_data\\lg_mail\\" + "downloaded_image.jpg";
		StringSelection clipboardString = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipboardString, null);
		
		Robot robot;
		try {
			robot = new Robot();
			
			if (!liteVersion.equals(true)) {
				robot.keyPress(KeyEvent.VK_V); // Save Image As...	 
				robot.keyRelease(KeyEvent.VK_V);
			}
			
			robot.delay(1000); // Save As dialog opens
			
			robot.keyPress(KeyEvent.VK_CONTROL); // Paste the download file path
			robot.keyPress(KeyEvent.VK_V);		 
			robot.keyRelease(KeyEvent.VK_V);		 
			robot.keyRelease(KeyEvent.VK_CONTROL);
			
			robot.keyPress(KeyEvent.VK_ENTER); // aka Save button
            robot.keyRelease(KeyEvent.VK_ENTER);
            
            robot.delay(3000); // Wait for file downloading...
            
        } catch (AWTException e) {
			e.printStackTrace();
		}
		
		return new File(filePath);
	}
	
	public Boolean validateDraftMessage(EmailMessage original, EmailMessage draftMessage) {
		
		if (!original.getReceiver().equals(draftMessage.getReceiver())) {
			//System.out.println(original.getReceiver() + " " + draftMessage.getReceiver());
			return false;
		}
		if (!original.getSubject().equals(draftMessage.getSubject())) {
			//System.out.println(original.getSubject() + " " + draftMessage.getSubject());
			return false;
		}
		if (!original.getBody().equals(draftMessage.getBody())) {
			//System.out.println(original.getBody() + " " + draftMessage.getBody());
			return false;
		}
		if (!original.getAttachments().get(0).getFileName().equals(draftMessage.getAttachments().get(0).getFileName())) {
			//System.out.println(original.getAttachments().get(0).getFileName() + " " + draftMessage.getAttachments().get(0).getFileName());
			return false;
		}
			
		return true;
	}

	public Boolean validateReceivedMesage(EmailMessage original, EmailMessage receivedMessage) {
		
		if (!original.getReceiver().equals(receivedMessage.getReceiver())) {
			//System.out.println(original.getReceiver() + " " + receivedMessage.getReceiver());
			return false;
		}
		if (!original.getSubject().equals(receivedMessage.getSubject())) {
			//System.out.println(original.getSubject() + " " + receivedMessage.getSubject());
			return false;
		}
		if (!original.getBody().equals(receivedMessage.getBody())) {
			//System.out.println(original.getBody() + " " + receivedMessage.getBody());
			return false;
		}
		
		return true;
	}

	public void cleanUp(EmailMessage receivedMessage, @Optional File file) {
		
		CustomElement ce = new CustomElement(String.format(emailBySubject, receivedMessage.getSubject()));
		ce.click(); // Select test email
		deleteMessageBtn.click();
		System.out.println("Test mail deleted!");
		
		file.delete();
		System.out.println("Downloaded image deleted!");
	}
}
