package vietjet;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import elements.CustomElement;
import utils.DateTimeHelper;
import vietjet.VietJetEnum.SeatType;

public class SelectTravelOptionsPage extends GeneralPage {
	
	BookingSummarySection ticketSummary = new BookingSummarySection();
	
	public boolean result = true;
	
	// Dynamic Elements
	
	private String departTicketPrices = "//div[@id='toDepDiv']//td[@data-familyid='%s']";
	private String departTicketRadioBtn = "(//div[@id='toDepDiv']//td[@data-familyid='%s']//input[@id='gridTravelOptDep'])[%s]";
	private String returnTicketPrices = "//div[@id='toRetDiv']//td[@data-familyid='%s']";
	private String returnTicketRadioBtn = "(//div[@id='toRetDiv']//td[@data-familyid='%s']//input[@id='gridTravelOptRet'])[%s]";
	
	// Static Elements
	private CustomElement resultTitle = new CustomElement("//h1[not(@class) and not(@style)]");
	private CustomElement noFlightsFoundLabel = new CustomElement("//p[@class='ErrorCaption']");	
	private CustomElement continueButton = new CustomElement("//a[contains(@href,'continue')]");
	
	// Methods
	
	public SelectTravelOptionsPage() {
		noFlightsFound();
	}
	
	public void noFlightsFound() {
		if(noFlightsFoundLabel.isDisplayed(10)) {
			log.printMessage("There are no flights available for your specifications");
			Assert.assertEquals(false, true);
		}
	}
	
	public void isPageTitleDisplayCorrectly(String title) {
		if(!title.trim().equals(resultTitle.getText().trim())) {
			log.printMessage("Page Title displays incorrectly. Expected is '" + title.trim() + "' but OBSERVED IS: '" + resultTitle.getText().trim() + "'");
			result = result && false;
		}
	}	
	
	public void isTicketInfoCorrect(Ticket ticket) {
		ticketSummary.areDepartAirPortsDisplayCorrectly(ticket.getDepartFrom(),ticket.getReturnFrom());
		ticketSummary.areReturnAirPortsDisplayCorrectly(ticket.getReturnFrom(), ticket.getDepartFrom());
		ticketSummary.isCurrencyDisplayCorrectly(ticket.getCurrency());
		ticketSummary.isDepartDateDisplayedCorrectly(DateTimeHelper.formatDate(ticket.getDepartDate(),"dd/MM/yyyy E"));
		ticketSummary.isReturnDateDisplayedCorrectly(DateTimeHelper.formatDate(ticket.getReturnDate(),"dd/MM/yyyy E"));
		ticketSummary.isAdultDisplayCorrectly(ticket.getAdult());
		result = ticketSummary.result;		
	}
	
	private int findCheapestTicketIndex(String xpath,SeatType type) {		
		List<WebElement> elements = new CustomElement().findElements(String.format(xpath, type.getSeatType()));
		int ticketIndex = 0;
		if (elements.size()>1) {
			int price1 = Integer.parseInt(elements.get(ticketIndex).getText().split(" ")[0].trim().replace(",", ""));
			for (int i = 0; i <= elements.size()-2; i++) {				
				int price2 = Integer.parseInt(elements.get(i+1).getText().split(" ")[0].trim().replace(",", ""));
				if (price1>price2) {
					ticketIndex = i+1;
					price1 = Integer.parseInt(elements.get(ticketIndex).getText().split(" ")[0].trim().replace(",", ""));
				}
			}
		}
		return ticketIndex + 1;
	}
	
	public void selectTickets(SeatType type) {
		continueButton.waitForClickable();
		new CustomElement(String.format(departTicketRadioBtn, type.getSeatType(), findCheapestTicketIndex(departTicketPrices,type))).click();
		//resultTitle.moveMouse();
		new CustomElement(String.format(returnTicketRadioBtn, type.getSeatType(), findCheapestTicketIndex(returnTicketPrices,type))).click();
		continueButton.click();
	}
}
