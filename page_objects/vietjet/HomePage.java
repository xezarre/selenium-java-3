package vietjet;

import java.time.LocalDate;

import elements.ComboBox;
import elements.CustomElement;
import utils.DateTimeHelper;
import vietjet.Ticket;
import vietjet.VietJetEnum.DirectionType;

public class HomePage extends GeneralPage {
	
	// String for Dynamic Elements
	
	private String travelRadioLabel = "//label[text()='%s']/preceding-sibling::input";
	private String targetDate = "//div[@id='ui-datepicker-div']//a[.='%s']";
	private String findLowestFare = "//label[text()='%s']/preceding-sibling::input";
	
	// Search Flight elements
	
	private CustomElement bookFlightTab = new CustomElement("//ul[@class='flight']//img[@alt='Flight']/ancestor::li");
	private CustomElement checkinTab = new CustomElement("//ul[@class='flight']//img[@alt='Checkin']");
	private CustomElement hotelTab = new CustomElement("//ul[@class='flight']//img[@alt='hotel']");
	
	private CustomElement departDateTextbox = new CustomElement("//input[contains(@id,'DepartDate')]");
	private CustomElement returnDateTextbox = new CustomElement("//input[contains(@id,'ReturnDate')]");	
	
	private ComboBox departComboBox = new ComboBox("//select[@id='selectOrigin']");
	private ComboBox returnComboBox = new ComboBox("//select[@id='selectDestination']");
	
	private CustomElement searchButton = new CustomElement("//input[contains(@id,'BtSearch')]");
	
	// Date-time Picker Elements 
	
	private ComboBox monthComboBox = new ComboBox("//div[@id='ui-datepicker-div']//select");
	private CustomElement yearLabel = new CustomElement("//div[@id='ui-datepicker-div']//span[@class='ui-datepicker-year']");
	private CustomElement nextButton = new CustomElement("//div[@id='ui-datepicker-div']//span[.='Next']");
	private CustomElement backButton = new CustomElement("//div[@id='ui-datepicker-div']//span[.='Prev']");
	
	// Return Dynamic Element methods
	
	private CustomElement getTravelRadioButton(String buttonLabel) {
		return new CustomElement(String.format(travelRadioLabel, buttonLabel));		
	}
	
	private CustomElement getLowestFareCheckbox(String buttonLabel) {
		return new CustomElement(String.format(findLowestFare, buttonLabel));		
	}
	
	private CustomElement getDateInPicker(String date) {
		return new CustomElement(String.format(targetDate, date));		
	}	
	
	// Main Methods 
	
	public SelectTravelOptionsPage searchTicket (Ticket ticket) {		
		this.selectService("Flight");
		this.setTravelType(ticket.getFlightType());
		this.selectAirport(ticket.getDepartFrom(), DirectionType.DEPART);
		this.selectAirport(ticket.getReturnFrom(), DirectionType.RETURN);
		if(ticket.getLowestFare().equals("no")) {
			this.selectDate(ticket.getDepartDate(), DirectionType.DEPART);
			this.selectDate(ticket.getReturnDate(), DirectionType.RETURN);
		}else {
			selectLowestFare(ticket.getLowestFareLabel());
		}
		this.selectCurrency(ticket.getCurrency());
		this.selectPassengerNumber(ticket.getAdult());
		searchButton.click();		
		return new SelectTravelOptionsPage();
	}
	
	private void selectLowestFare(String label) {
		if(!getLowestFareCheckbox(label).isSelected()) {
			getLowestFareCheckbox(label).click();
		}
	}
	
	private void selectCurrency(String currency) {
		log.printMessage("Skip this action due to cannot interate with Currency Cobobox");		
	}

	private void selectPassengerNumber(Integer adult) {
		
		new CustomElement("//button[@id='ctl00_UcRightV31_CbbAdults_Button']").click();
		new CustomElement(String.format("//ul[@id=\"ctl00_UcRightV31_CbbAdults_OptionList\"]//*[text()='%s']", adult)).click();
	}

	private void setTravelType(String type) {
		this.getTravelRadioButton(type).click();
	}
	
	private void selectService(String service) {
		
		switch(service) {
		case "Flight":
			bookFlightTab.click();
			break;
		case "Checkin":
			checkinTab.click();
			break;
		case "Hotel":
			hotelTab.click();
			break;
		}
	}
	
	private void switchToTargetDate(LocalDate date) {
		int targetYear = Integer.parseInt(DateTimeHelper.formatDate(date, "yyyy"));
		int displayedYear = Integer.parseInt(yearLabel.getText());		
		// Go to target year
		if(targetYear != displayedYear) {
			// Click next button
			if(displayedYear < targetYear) {
				do {
					this.nextButton.click();
					displayedYear = Integer.parseInt(yearLabel.getText());
				} while (displayedYear < targetYear);
			} else if (displayedYear > targetYear) {
				do {
					this.backButton.click();
					displayedYear = Integer.parseInt(yearLabel.getText());
				} while (displayedYear > targetYear);
			}
		}		
		// Go to target month
		String targetMonth = DateTimeHelper.formatDate(date, "MMM");		
		if(!targetMonth.equals(monthComboBox.getSelected())) {
			monthComboBox.selectByVisibleText(targetMonth);
		}			
		getDateInPicker(DateTimeHelper.formatDate(date, "d")).click();
	}
	
	/**
	 * Select Depart Date or Return Date on Datetime Picker.
	 * Flight Type (Depart, Return)
	 * @author: Hien Khuu
	 */
	private void selectDate(LocalDate date, DirectionType type) {		
		if (date == null) {
			log.printMessage("There is no Depart Date. Skip this step");
		} else if(date.compareTo(DateTimeHelper.getCurrentDate()) >= 0) {
			if(type.getDirection().equals("Depart")) {
				this.departDateTextbox.click();
			} else if(type.getDirection().equals("Return")) {
				this.returnDateTextbox.click();
			} else {
				log.printMessage("Incorrect Flight Type. Please recheck");
			}			
			this.switchToTargetDate(date);
		} else if(date.compareTo(DateTimeHelper.getCurrentDate()) < 0) {
			log.printMessage("Cannot select YEAR IN PAST.");
		}
	}
	
	/**
	 * Select Airport from combobox.
	 * Flight Type (Depart, Return)
	 * @author: Hien Khuu
	 */
	private void selectAirport(String airPort, DirectionType type) {
		if(type.getDirection().equals("Depart")) {
			this.departComboBox.selectByVisibleText(airPort);
		} else if(type.getDirection().equals("Return")) {
			this.returnComboBox.selectByVisibleText(airPort);
		} else {
			log.printMessage("Incorrect Flight Type. Please recheck");
		}		
	}

}
