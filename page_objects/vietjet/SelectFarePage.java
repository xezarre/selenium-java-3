package vietjet;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import elements.CustomElement;
import utils.DateTimeHelper;
import vietjet.VietJetEnum.*;

public class SelectFarePage extends GeneralPage{
	
	BookingSummarySection ticketSummary = new BookingSummarySection();
	
	private String dayFlight = "//div[@id='vv%sDiv']//td[contains(@class,'Low')]";
	private String priceFlight = "(//div[@id='vv%sDiv']//td[contains(@class,'Low')]//p)[%s]";
	private String nextButton = "//div[@id='vv%sDiv']//th[contains(@class,'Next')]/a";
	private String backButton = "//div[@id='vv%sDiv']//th[contains(@class,'Prev')]/a";
	private String targetDate = "//div[@id='vv%sDiv']//td[contains(@id,'%s')]/div";
	private String selectedDate = "//div[@id='vv%sDiv']//td[contains(@class,'Selected')]";
	
	private CustomElement continueButton = new CustomElement("//a[contains(@href,'continue')]");
	
	public boolean result = true;
	
	private CustomElement getNextButton(DirectionType type) {
		return new CustomElement(String.format(nextButton, type.getShortcutDirection()));
	}
	
	private CustomElement getBackButton(DirectionType type) {
		return new CustomElement(String.format(backButton, type.getShortcutDirection()));
	}
	
	private CustomElement getTargetDate(DirectionType type, String date) {
		return new CustomElement(String.format(targetDate, type.getShortcutDirection(),date));
	}
	
	private CustomElement getSelectedDate(DirectionType type) {
		return new CustomElement(String.format(selectedDate, type.getShortcutDirection()));
	}
	
	public void verifyTicketInfoWithoutDate(Ticket ticket) {
		ticketSummary.areDepartAirPortsDisplayCorrectly(ticket.getDepartFrom(),ticket.getReturnFrom());
		ticketSummary.areReturnAirPortsDisplayCorrectly(ticket.getReturnFrom(), ticket.getDepartFrom());
		ticketSummary.isCurrencyDisplayCorrectly(ticket.getCurrency());
		ticketSummary.isAdultDisplayCorrectly(ticket.getAdult());
		result = ticketSummary.result;		
	}
	
	public void selectCheapestTour(Ticket tk) {
		ArrayList<String> targetTour = this.getCheapestTourList(tk.getMonthAmount(), tk.getTripDuration());
		switchToDayFlight(DirectionType.DEPART, targetTour.get(0));
		switchToDayFlight(DirectionType.RETURN, targetTour.get(1));

		getTargetDate(DirectionType.DEPART, targetTour.get(0)).click();
		getTargetDate(DirectionType.RETURN, targetTour.get(1)).click();
		
		getSelectedDateFromCalendar(DirectionType.DEPART);
		getSelectedDateFromCalendar(DirectionType.RETURN);
		
		tk.setDepartDate(DateTimeHelper.convertStrToDate(getSelectedDateFromCalendar(DirectionType.DEPART),"yyyy/MM/dd"));
		tk.setReturnDate(DateTimeHelper.convertStrToDate(getSelectedDateFromCalendar(DirectionType.RETURN),"yyyy/MM/dd"));
		
		continueButton.click();
	}
	
	private ArrayList<String> getCheapestTourList(int findTicketsWithinMonth, int tripDuration) {
		//Scan all days within input months to get prices 
		ArrayList<ArrayList<Integer>> departList = new ArrayList<>();
		ArrayList<ArrayList<Integer>> returnList = new ArrayList<>();
		
		for(int loop=1; loop<=findTicketsWithinMonth; loop++) {
			continueButton.waitForClickable(60);			
			departList.addAll(getCheapestFlightList(DirectionType.DEPART));
			returnList.addAll(getCheapestFlightList(DirectionType.RETURN));
			if(loop<findTicketsWithinMonth) {
				getNextButton(DirectionType.DEPART).click();
				continueButton.waitForClickable(60);
				getNextButton(DirectionType.RETURN).click();
			}
		}
		getNextButton(DirectionType.RETURN).click();
		continueButton.waitForClickable(60);
		returnList.addAll(getCheapestFlightList(DirectionType.RETURN));		
		// get the couple cheapest tour
		int cheapestPrice = Integer.MAX_VALUE;
		int depIndex = 0;
		int retIndex = 0;
		for (int d = 0; d <= departList.size()-1; d++) {
			for (int r = 0; r <= returnList.size()-1; r++) {
				if((departList.get(d).get(1)+returnList.get(r).get(1)<cheapestPrice) && (returnList.get(r).get(0)-departList.get(d).get(0)==tripDuration)) {
					cheapestPrice = departList.get(d).get(1)+returnList.get(r).get(1);
					depIndex = d;
					retIndex = r;
				}
			}
		}
		//Return tour
		ArrayList<String> tour = new ArrayList<String>();
		tour.add(departList.get(depIndex).get(0).toString());
		tour.add(returnList.get(retIndex).get(0).toString());
		return tour;
	}
	
	private ArrayList<ArrayList<Integer>> getCheapestFlightList(DirectionType type) {
		List<WebElement> dayFlights = new CustomElement().findElements(String.format(dayFlight, type.getShortcutDirection()));
		ArrayList<ArrayList<Integer>> flightList = new ArrayList<>();
		for (int i = 0; i <= dayFlights.size()-1; i++) {
			
			int dayPrice = Integer.parseInt(new CustomElement(String.format(priceFlight, type.getShortcutDirection(),i+1)).getText().split(" ")[0].trim().replace(",", ""));
			int dayFormat = Integer.parseInt(dayFlights.get(i).getAttribute("id").substring(17));
			
			flightList.add(new ArrayList<Integer>());
			flightList.get(i).add(0,dayFormat);
			flightList.get(i).add(1,dayPrice);
		}
		return flightList;
	}
	
	private void switchToDayFlight(DirectionType type, String date) {
		if(!getTargetDate(type,date).isDisplayed()) {
			do {
				getBackButton(type).click();
				continueButton.waitForClickable(60);
			} while(!getTargetDate(type,date).isDisplayed());			
		}
	}
	
	private String getSelectedDateFromCalendar(DirectionType type) {
		String tempStr = getSelectedDate(type).getAttribute("onclick").split("='")[1];
		String date = tempStr.split(",")[0];
		return date;
	}
}
