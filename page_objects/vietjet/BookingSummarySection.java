package vietjet;

import elements.CustomElement;

public class BookingSummarySection extends GeneralPage{

	protected boolean result = true;
	
	// Booking Summary 
	private CustomElement departFromLabel = new CustomElement("(//table[@id='tblLeg1APs']//td)[1]");
	private CustomElement departToLabel = new CustomElement("(//table[@id='tblLeg1APs']//td)[2]");
	private CustomElement returnFromLabel = new CustomElement("(//table[@id='tblLeg2APs']//td)[1]");
	private CustomElement returnToLabel = new CustomElement("(//table[@id='tblLeg2APs']//td)[2]");
	
	private CustomElement currencyLabel = new CustomElement("//span[@class='BSGrandTotal' and not(@id)]");
	private CustomElement departDateLabel = new CustomElement("//span[@id='Leg1Date']");
	private CustomElement returnDateLabel = new CustomElement("//span[@id='Leg2Date']");
	public CustomElement price = new CustomElement("//div[@id='toDepDiv']//td[@data-familyid='Eco']");
	
	private CustomElement adultLabel = new CustomElement("//table[@id='tblPaxCountsInfo']//span[contains(.,'Adults')]/ancestor::td");

	
	private String formatTicketAirport(String airport) {
		return airport.split("\\(")[0].trim();
	}
	
	private String formatObservedAirport(String airport) {
		return airport.split(":")[1].trim();
	}	
	
	protected void areDepartAirPortsDisplayCorrectly(String depart, String arrive) {
		
		String observedDepart = formatObservedAirport(departFromLabel.getText());
		String observedReturn = formatObservedAirport(departToLabel.getText());
		String expectedDepart = formatTicketAirport(depart);
		String expectedReturn = formatTicketAirport(arrive);		
				
		if (!observedDepart.equals(expectedDepart)) {
			log.printMessage("Depart AirPort displays incorrectly. Expected is '" + expectedDepart + "' but OBSERVED IS: '" + observedDepart + "'");
			result = result && false;
		} else if (!observedReturn.equals(expectedReturn)) {
			log.printMessage("Return AirPort displays incorrectly. Expected is '" + expectedReturn + "' but OBSERVED IS: '" + observedReturn + "'");
			result = result && false;
		}
	}
	
	protected void areReturnAirPortsDisplayCorrectly(String depart, String arrive) {
		
		String observedDepart = formatObservedAirport(returnFromLabel.getText());
		String observedReturn = formatObservedAirport(returnToLabel.getText());
		String expectedDepart = formatTicketAirport(depart);
		String expectedReturn = formatTicketAirport(arrive);	
				
		if (!observedDepart.equals(expectedDepart)) {
			log.printMessage("Depart AirPort displays incorrectly. Expected is '" + expectedDepart + "' but OBSERVED IS: '" + observedDepart + "'");
			result = result && false;
		} else if (!observedReturn.equals(expectedReturn)) {
			log.printMessage("Return AirPort displays incorrectly. Expected is '" + expectedReturn + "' but OBSERVED IS: '" + observedReturn + "'");
			result = result && false;
		}
	}
	
	protected void isCurrencyDisplayCorrectly(String currency) {
		
		if (!currencyLabel.getText().trim().equals(currency.trim())) {
			log.printMessage("Currency displays incorrectly. Expected is '" + currency.trim() + "' but OBSERVED IS: '" + currencyLabel.getText().trim() + "'");
			result = result && false;
		}
	}
	
	protected void isDepartDateDisplayedCorrectly(String date) {
		
		if (!date.trim().equals(departDateLabel.getText().trim())) {
			log.printMessage("Depart Date displays incorrectly. Expected is '" + date.trim() + "' but OBSERVED IS: '" + departDateLabel.getText().trim() + "'");
			result = result && false;
		}
	}
	
	protected void isReturnDateDisplayedCorrectly(String date) {
		
		if (!date.trim().equals(returnDateLabel.getText().trim())) {
			log.printMessage("Return Date displays incorrectly. Expected is '" + date.trim() + "' but OBSERVED IS: '" + returnDateLabel.getText().trim() + "'");
			result = result && false;
		}
	}
	
	protected void isAdultDisplayCorrectly(double amount) {		
		Double observed = Double.parseDouble(adultLabel.getText().split(":")[1].trim());
		
		if (observed != amount) {
			log.printMessage("Adults display incorrectly. Expected is '" + amount + "' but OBSERVED IS: '" + observed + "'");
			result = result && false;
		}		
	}
}
