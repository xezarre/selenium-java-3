package agoda;

import java.util.ArrayList;
import driver.DriverManager;
import utils.Logger;

public class GeneralPage {

	Logger log = new Logger();
	
	ArrayList<String> handler = DriverManager.getDriver().getWindowHandles();
	
	public void closeTab() {
		DriverManager.getDriver().close();
		DriverManager.getDriver().switchTo(handler.get(0));
	}
}
