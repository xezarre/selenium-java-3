package agoda;

import agoda.HomePageEnums.*;
import elements.CustomElement;

public class HomePage extends GeneralPage {
	
	private String dateControl = "//*[@aria-label='%s']";
	private String adultElement = "//*[@data-selenium='occupancyAdults']/*[@data-selenium='%s']";
	private String roomElement = "//*[@data-selenium='occupancyRooms']/*[@data-selenium='%s']";
	private String serviceTabElement = "//*[@data-selenium='%s']";
	private String travelTypeOption = "//*[@data-selenium='%s']";
	private String destinationSuggestedOption = "//*[@data-selenium='suggestion-text-highlight' and text()='%s']";
	
	private CustomElement destinationTxt = new CustomElement("//input[@data-selenium='textInput']");
	private CustomElement datePickerCaption = new CustomElement("(//*[@class='DayPicker-Caption']/div)[1]");
	private CustomElement previousMonthBtn = new CustomElement("//*[@data-selenium='calendar-previous-month-button']");
	private CustomElement nextMonthBtn = new CustomElement("//*[@data-selenium='calendar-next-month-button']");
	private CustomElement searchBtn = new CustomElement("//*[@data-selenium='searchButton']");
	
	public SearchResultPage search(SearchCriteria sc) {
		
		// Select Criterias
		this.selectServiceTab(Services.getServicesByName(sc.getCategory()))
			.selectDestination(sc.getDestination())
			.selectDate(sc.getStartDate())
			.selectDate(sc.getEndDate())
			.selectTravelType(TravelTypes.getTravelTypeByName(sc.getTravelType()))
			.selectAdult(sc.getAdult())
			.selectRoom(sc.getRoom());
		
		// Click SEARCH button.
		searchBtn.click();
		
		return new SearchResultPage();
	}
	
	private HomePage selectRoom(Integer expectedValue) {
		
		CustomElement ce = null;
		int currentValue = Integer.parseInt(new CustomElement(String.format(roomElement, Room.VALUE.getValue())).getText());;
		if (currentValue < expectedValue) {
			ce = new CustomElement(String.format(roomElement, Room.PLUS.getValue()));
			for (int i = currentValue; i < expectedValue; i++) { ce.click(); }
		} else {
			ce = new CustomElement(String.format(roomElement, Room.MINUS.getValue()));
			for (int i = expectedValue; i > currentValue; i++) { ce.click(); }
		}
		return this;
	}

	private HomePage selectAdult(Integer expectedValue) {
		
		CustomElement ce = null;
		int currentValue = Integer.parseInt(new CustomElement(String.format(adultElement, Adult.VALUE.getValue())).getText());;
		if (currentValue < expectedValue) {
			ce = new CustomElement(String.format(adultElement, Adult.PLUS.getValue()));
			for (int i = currentValue; i < expectedValue; i++) { ce.click(); }
		} else {
			ce = new CustomElement(String.format(adultElement, Adult.MINUS.getValue()));
			for (int i = expectedValue; i > currentValue; i++) { ce.click(); }
		}
		return this;
	}

	private HomePage selectTravelType(TravelTypes type) {
		CustomElement ce = new CustomElement(String.format(travelTypeOption, type.getValue()));
		ce.click();
		return this;
	}

	private HomePage selectServiceTab(Services service) {
		CustomElement ce = new CustomElement(String.format(serviceTabElement, service.getValue()));
		ce.click();
		return this;
	}
	
	private HomePage selectDestination(String destination) {
		destinationTxt.enter(destination);
		new CustomElement(String.format(destinationSuggestedOption, destination)).click();
		return this;
	}
	
	private HomePage selectDate(String date) {
		
		// Handle Month picker
		Integer monthDisplay = Months.getMonthByName(datePickerCaption.getText().trim().substring(0, 3)).getValue();
		Integer monthExpect = Months.getMonthByName(date.substring(4, 7)).getValue();
		
		if (monthDisplay > monthExpect) {
			for (int i = monthExpect; i < monthDisplay; i++) {
				previousMonthBtn.click();
			}
		}
		
		if (monthDisplay < monthExpect) {
			for (int i = monthDisplay; i < monthExpect; i++) {
				nextMonthBtn.click();
			}
		}
		
		CustomElement dateElement = null;
		dateElement = new CustomElement(String.format(dateControl, date));
		dateElement.click();
		
		return this;
	}
}
