package agoda;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import elements.CustomElement;
import utils.Logger;

public class SearchResultPage_Thuy {

	private CustomElement lnkSortBestMatch = new CustomElement(
			"//div[@id='sort-bar']//a[@data-element-name='search-sort-recommended']");
	private CustomElement lnkSortCheapestPrice = new CustomElement(
			"//div[@id='sort-bar']//a[@data-element-name='search-sort-price']");
	private CustomElement lnkSortNearestTo = new CustomElement(
			"//div[@id='sort-bar']//a[@data-element-name='search-sort-distance-landmark']");
	private CustomElement lnkSortTopReviewed = new CustomElement(
			"//div[@id='sort-bar']//a[@data-element-name='search-sort-guest-rating']");
	private CustomElement lnkSortSecretDeals = new CustomElement(
			"//div[@id='sort-bar']//a[@data-element-name='search-sort-secret-deals']");
	private CustomElement lnkClearBudgetFilter = new CustomElement("//div[@id='PriceFilterRange']//a[text()='CLEAR']");
	private CustomElement txtMinPrice = new CustomElement("//input[@id='price_box_0']");
	private CustomElement txtMaxPrice = new CustomElement("//input[@id='price_box_1']");
	private CustomElement btnYourBudgetFilter = new CustomElement(
			"//div[@data-element-name='search-filter-type-pricefilterrange']");
	private CustomElement btnStarsFilter = new CustomElement(
			"//div[@data-element-name='search-filter-type-starrating']");
	private CustomElement icoMinSlider = new CustomElement("//div[@class='rc-slider-handle rc-slider-handle-1']");
	private CustomElement icoMaxSlider = new CustomElement("//div[@class='rc-slider-handle rc-slider-handle-2']");
	private CustomElement popupLeaveSite = new CustomElement("//div[@class='LeaveSitePopup-Container']");
	private CustomElement icoPopupLeaveSiteClose = new CustomElement(
			"//div[@class='LeaveSitePopup-Container']//span[@class='ficon ficon-16 ficon-x-icon ficon-line-close close-button-top']");

	// Dynamic XPath
	private final String xpathIcoStar = "(//li[@data-selenium='hotel-item'])[%s]//i[contains(@class,'ficon ficon-star')]";
	private final String xpathLnkDestination = "(//li[@data-selenium='hotel-item'])[%s]//span[@class='areacity-name-text']";
	private final String xpathLblPrice = "(//li[@data-selenium='hotel-item'])[%s]//span[@class='price-box__price__amount']";
	// private final String xpathBtnFilterBy = "//button[@class='btn
	// PillDropdown__Button']//span[text()='%s']";
	private final String xpathChkStarNumber = "//ul[@class='list-filter__items filter-list-item']//span[@data-element-value='%s']//span[@class='checkbox-icon']";
	private String hotelItems = "//*[@data-selenium='hotel-item']";

	// Methods Section
	/**
	 * @author Thuy Tran
	 * @return The xpath for filter star number
	 */
	protected CustomElement getChkStarNumber(String star) {
		return new CustomElement(String.format(xpathChkStarNumber, star));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath for number of Star
	 */
	protected CustomElement getIcoStar(int hotelOrder) {
		return new CustomElement(String.format(xpathIcoStar, hotelOrder));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath for hotel destination
	 */
	protected CustomElement getLnkDestination(int hotelOrder) {
		return new CustomElement(String.format(xpathLnkDestination, hotelOrder));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath for Hotel price
	 */
	protected CustomElement getLblPrice(int hotelOrder) {
		return new CustomElement(String.format(xpathLblPrice, hotelOrder));
	}

	/**
	 * @author Thuy Tran
	 * @return Filter by budget
	 */
	protected void filterByBudget(int min, int max) {
		String priceMin = Integer.toString(min);
		String priceMax = Integer.toString(max);
		CustomElement filterBar = new CustomElement("//*[@class='filter-menu']");

		btnYourBudgetFilter.click();
		txtMinPrice.enter(priceMin);
		filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));

		txtMaxPrice.enter(priceMax);
		filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));
	}

	/**
	 * @author Thuy Tran
	 * @return Filter by star
	 */
	protected void filterByStar(String star) {
		btnStarsFilter.click();
		getChkStarNumber(star).set(true);
		btnStarsFilter.click();
	}

	/**
	 * @author Thuy Tran
	 * @return Clear filter
	 */
	protected void clearFilter(String filterOption) {
		btnYourBudgetFilter.waitForClickable(20);
		btnYourBudgetFilter.click();
		lnkClearBudgetFilter.click();
	}

	/**
	 * @author Thuy Tran
	 * @return Get current value of min and max range of slider
	 */

	public List<String> getCurrentSliderPriceRange() {
		List<String> priceRange = new ArrayList<String>();
		priceRange.add(icoMinSlider.getAttribute("aria-valuenow"));
		priceRange.add(icoMaxSlider.getAttribute("aria-valuenow"));
		return priceRange;
	}

	/**
	 * @author Thuy Tran
	 * @return Get default value of min and max range of slider
	 */

	public List<String> getDefaultSliderPriceRange() {
		List<String> priceRange = new ArrayList<String>();
		priceRange.add(icoMinSlider.getAttribute("aria-valuemin"));
		priceRange.add(icoMaxSlider.getAttribute("aria-valuemax"));
		return priceRange;
	}

	/**
	 * @author Thuy Tran
	 * @return Get Hotel info in Search Result page
	 */

	public Hotel getHotelResultInfo(int hotelOrder) {
		Logger logger = new Logger();
		Hotel hotel = new Hotel();
		String star = "", destination, price = "";

		lnkSortCheapestPrice.clickAndWaitForPageLoaded();
		getLnkDestination(hotelOrder).waitForClickable(10);
		getLnkDestination(hotelOrder).scrollToElement(
				"(//li[@data-selenium='hotel-item'])[" + hotelOrder + "]//span[@class='areacity-name-text']");
		if (getIcoStar(hotelOrder).isExisted() != false) {
			star = getIcoStar(hotelOrder).getAttribute("title");
		}
		destination = getLnkDestination(hotelOrder).getText();
		if (getLblPrice(hotelOrder).isExisted() != false) {
			price = getLblPrice(hotelOrder).getText();
		}
		hotel.setStar(star);
		hotel.setDestination(destination);
		hotel.setPrice(price);
		logger.printMessage("star:" + hotel.getStar());
		logger.printMessage("destination:" + hotel.getDestination());
		logger.printMessage("price:" + hotel.getPrice());
		
		return hotel;
	}

	/**
	 * @author Thuy Tran
	 * @return Get info of 5 hotels in Search Result page
	 */

	public List<String> getPriceOfMultipleHotels(int numberOfHotel) {
		List<String> displayed = new ArrayList<String>();
		int listSize = displayed.size();
		int i = 1;
		String price = "";

		while (listSize < numberOfHotel) {
			price = getHotelResultInfo(i).getPrice();
			if (price != "") {
				displayed.add(price.replace(",", ""));
			}
			listSize = displayed.size();
			i = i + 1;
		}

		if (popupLeaveSite.isDisplayed()) {
			icoPopupLeaveSiteClose.click();
		}
		getLnkDestination(0).scrollToElement(
				"(//li[@data-selenium='hotel-item'])[1]//span[@class='areacity-name-text']");
		return displayed;
	}

	/**
	 * @author Thuy Tran
	 * @return Check if sorting by cheapest price is correct or not
	 */

	public Boolean isSortByCheapestPriceCorrectly(List<String> priceList) {
		for (int i = 0; i < priceList.size() - 1; i++) {
			if ((Integer.parseInt(priceList.get(i + 1).toString())) < (Integer.parseInt(priceList.get(i).toString()))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @author Thuy Tran
	 * @return Check if the price is in range of filter
	 */

	public Boolean isPriceInRange(List<String> priceList) {
		for (int i = 0; i < priceList.size() - 1; i++) {
			if ((Integer.parseInt(priceList.get(i + 1).toString())) < (Integer.parseInt(priceList.get(i).toString()))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @author Thuy Tran
	 * @return Sort hotel by Best Match options
	 */

	public void sortByRecommended() {
		lnkSortBestMatch.click();
	}

	/**
	 * @author Thuy Tran
	 * @return Sort hotel by Cheapest Price options
	 */

	public void sortByCheapestPrice() {

		lnkSortCheapestPrice.clickAndWaitForPageLoaded();
	}

	/**
	 * @author Thuy Tran
	 * @return Sort hotel by Top Reviewed options
	 */

	public void sortByTopReviewed() {
		lnkSortTopReviewed.click();
	}

	/**
	 * @author Thuy Tran
	 * @return Sort hotel by Nearest To options
	 */

	public void sortByNearestTo() {
		lnkSortNearestTo.click();
	}

	/**
	 * @author Thuy Tran
	 * @return Sort hotel by Secret Deals options
	 */

	public void sortBySecretDeals() {
		lnkSortSecretDeals.click();
	}
}