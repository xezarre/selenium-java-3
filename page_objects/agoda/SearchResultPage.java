package agoda;

import java.util.ArrayList;

import org.openqa.selenium.By;

import agoda.SearchResultPageEnums.FilterBy;
import agoda.SearchResultPageEnums.OrderBy;
import agoda.SearchResultPageEnums.Rating;
import agoda.SearchResultPageEnums.ReviewPoints;
import agoda.SearchResultPageEnums.SortBy;
import elements.CustomElement;
import utils.Utilities;

public class SearchResultPage extends GeneralPage {
	
	public Boolean RESULT = true;
	private final String HIGHLIGHT_COLOR = "#425ff9";
	private final String UNHIGHLIGHTED_COLOR = "#20274d";
	
	private String sortOption = "//div[@id='sort-bar']//a[@data-element-name='%s']";
	private String filterOption = "//div[@data-element-name='%s']//button";
	private String ratingChbx = "//li/*[@data-element-value='%s']";
	private String facilityOption = "//*[@data-selenium='filter-item-text' and text()='%s']";
	private String clearPriceFilterBtn = "//*[@data-element-name='search-filter-type-pricefilterrange-close']";
	
	private CustomElement priceMinTxt = new CustomElement("//input[@id='price_box_0']");
	private CustomElement priceMaxTxt = new CustomElement("//input[@id='price_box_1']");
	
	//private CustomElement closePopupBtn = new CustomElement("//div[@class='LeaveSitePopup-CloseArea']//*[@data-element-name='leave-site-popup-close-button']");
	
	private String hotelItems = "//*[@data-selenium='hotel-item']";
	private String itemSoldOut = "//*[@data-selenium='hotel-item'][%s]//*[@class='sold-out-message'])";
	private String hotelScore = "//*[@data-selenium='hotel-item'][%s]//*[@class='ReviewScore-Number']";
	private String hotelName = "(//*[@data-selenium='hotel-item']//*[@data-selenium='hotel-name'])[%s]";
	private String hoteRating = "(//*[@data-selenium='hotel-item']//*[@data-selenium='hotel-star-rating'])[%s]";
	private String hotelArea = "(//*[@data-selenium='hotel-item']//*[@data-selenium='area-city-text'])[%s]";
	private String hotelPrice = "(//*[@data-selenium='hotel-item']//*[@data-selenium='display-price'])[%s]";
	
	private String hotelReviewPane = "//*[text()='%s']/ancestor::li[@data-selenium='hotel-item']//*[@data-selenium='property-review-container']";
	
	/**
	 * Sort accommodation by input Type
	 * @author Vinh
	 * @param type
	 * @return
	 */
	public SearchResultPage sort(String type) {
		CustomElement ce = new CustomElement(String.format(sortOption, SortBy.getSortOptionBy(type)));
		ce.click();
		return this;
	}
	
	/**
	 * Filter accommodation by Price range.
	 * @author vinh
	 * @param minValue
	 * @param maxValue
	 * @return
	 */
	public SearchResultPage filterByPrice(Integer minValue, Integer maxValue) {
		
		CustomElement ce = new CustomElement(String.format(filterOption, FilterBy.BUDGET.getValue()));
		CustomElement filterBar = new CustomElement("//*[@class='filter-menu']");
		
		ce.click(); // open filter pane
		
		if (minValue == null) minValue = 0;
		if (maxValue == null) maxValue = 0;
		
		if (minValue < 0) minValue = Math.abs(minValue);
		if (maxValue < 0) maxValue = Math.abs(maxValue);
		
		if (minValue < maxValue) {
			
			priceMinTxt.enter(minValue.toString());
			filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));
			
			priceMaxTxt.enter(maxValue.toString());
			filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));
		} else if (minValue > maxValue) {
			
			priceMinTxt.enter(maxValue.toString());
			filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));
			
			priceMaxTxt.enter(minValue.toString());
			filterBar.clickAndWaitForElementsPresence(By.xpath(hotelItems));
		}
		
		ce.click(); // dismiss filter pane
		
		return this;
	}
	
	/**
	 * Clear Price filter
	 * @author Vinh
	 * @return
	 */
	public SearchResultPage clearPriceFilter() {
		
		new CustomElement().scrollElementIntoMiddle(clearPriceFilterBtn);
		new CustomElement(clearPriceFilterBtn).click();
		return this;
	}
	
	/**
	 * Filter accommodations by Rating
	 * @author Vinh
	 * @param rating
	 * @return
	 */
	public SearchResultPage filterByRating(String rating) {
		
		CustomElement ce = new CustomElement(String.format(filterOption, FilterBy.STAR.getValue()));
		ce.click(); // open filter pane
		
		CustomElement ratingOption = new CustomElement(String.format(ratingChbx, Rating.getRateOptionByName(rating)));
		ratingOption.click();
		
		ce.click();	// dismiss filter pane
		
		return this;
	}
	
	/**
	 * Filter accommodations by more Option
	 * @author Vinh
	 * @param option
	 * @return
	 */
	public SearchResultPage filterByMoreOption(String option) {
		
		Amenities.addOption(option);
		
		CustomElement ce = new CustomElement(String.format(filterOption, FilterBy.MORE.getValue()));
		ce.click(); // open filter pane
		
		CustomElement opt = new CustomElement(String.format(facilityOption, option));
		opt.click();
		
		ce.click(); // dismiss filter pane
		
		return this;
	}
	
	/**
	 * Get first number of accommodation displays on page.
	 * @author Vinh
	 * @param number
	 * @return
	 */
	public Accommodations getAccommodationsOf(Integer number) {
		
		Accommodations accs = new Accommodations();
		
		int i = 1; // Index of first accommodation appears on page.
		
		do {
			new CustomElement().waitForElementsPresence(By.xpath(hotelItems + "[" + i + "]"));
			
			if (!new CustomElement(String.format(itemSoldOut, i)).isDisplayed()) {
				
				Accommodation acc;
				
				if (!new CustomElement(String.format(hotelScore, i)).isDisplayed()) {
					acc = new Accommodation(new CustomElement(String.format(hotelName, i)).getText(),
											new CustomElement(String.format(hotelArea, i)).getText().split("-")[0].trim(),
											new CustomElement(String.format(hoteRating, i)).getAttribute("title"),
											new CustomElement(String.format(hotelPrice, i)).getText(),
											"Awaiting reviews");
				} else {
					acc = new Accommodation(new CustomElement(String.format(hotelName, i)).getText(),
											new CustomElement(String.format(hotelArea, i)).getText().split("-")[0].trim(),
											new CustomElement(String.format(hoteRating, i)).getAttribute("title"),
											new CustomElement(String.format(hotelPrice, i)).getText(),
											new CustomElement(String.format(hotelScore, i)).getText());
				}
				
				//System.out.println(acc.toString());
				
				accs.addAccommodation(acc);
				i++;
			} else {
				i++;
				number++;
			}
			// Scroll to next accommodation [i] to make sure the [i+1] being loaded 
			//new CustomElement().scrollToElement(String.format(hotelName, i)); 
			new CustomElement().scrollElementIntoMiddle(String.format(hotelName, i)); 
		} while (i <= number);
		
		return accs;
	}

	/**
	 * Verify that prices are in correct order
	 * @author Vinh
	 * @param order
	 * @return
	 */
	public SearchResultPage verifyPriceInOrder(Accommodations accs, OrderBy order) {
		
		ArrayList<Integer> price = new ArrayList<Integer>();
		
		accs.getList().forEach((ac) -> { 
			price.add(Integer.parseInt(ac.getPrice().replaceAll(",", ""))); 
		});
		
		if (order.getValue().equals("asc")) {
			for (int i = 0; i < price.size() - 1; i++) 
				if (price.get(i) > price.get(i + 1)) {
					RESULT = RESULT && false;
					log.printMessage("Accommodation prices are not in correct sort order!");
					break;
				}
		}
		
		if (order.getValue().equals("desc")) {
			for (int i = 0; i < price.size() - 1; i++) 
				if (price.get(i) < price.get(i + 1)) {
					RESULT = RESULT && false;
					log.printMessage("Accommodation prices are not in correct sort order!");
					break;
				}
		}
		return this;
	}
	
	/**
	 * Verify that location is correct.
	 * @author Vinh
	 * @param expectedLocation
	 * @return
	 */
	public SearchResultPage verifyAccommodateLocation(Accommodations accs, String expectedLocation) {
		
		ArrayList<String> location = new ArrayList<String>();
		
		accs.getList().forEach((ac) -> { 
			location.add(ac.getLocation());
		});
		
		for (int i = 0; i < location.size(); i++) {
			if (!location.get(i).contains(expectedLocation)) {
				RESULT = RESULT && false;
				log.printMessage("Accommodation: " + accs.getAccommodationAt(i).getName() + "has incorrect location!");
				break;
			}
		}
		
		return this;
	}

	/**
	 * Verify that rating is correct.
	 * @author Vinh
	 * @param rating
	 * @return
	 */
	public SearchResultPage verifyAccommodationRatingFrom(Accommodations accs, Rating rating) {
		
		ArrayList<String> rate = new ArrayList<String>();
		
		accs.getList().forEach((ac) -> { 
			rate.add(ac.getRate());
		});
		
		for (int i = 0; i < rate.size(); i++) {
			if (Double.parseDouble(rate.get(i).split(" ")[0]) < rating.getValue()) {
				RESULT = RESULT && false;
				log.printMessage("Accommodation: " + accs.getAccommodationAt(i).getName() 
							+ " has incorrect rating: " + accs.getAccommodationAt(i).getRate());
				break;
			}
		}
		
		return this;
	}

	/**
	 * Verify that prices are in range
	 * @author Vinh
	 * @param budgetMin
	 * @param budgetMax
	 * @return
	 */
	public SearchResultPage verifyPriceInRange(Accommodations accs, Integer budgetMin, Integer budgetMax) {
		
		ArrayList<Integer> price = new ArrayList<Integer>();
		
		accs.getList().forEach((ac) -> { 
			price.add(Integer.parseInt(ac.getPrice().replaceAll(",", ""))); 
		});
		
		for (int i = 0; i < price.size(); i++) {
			if (budgetMin > price.get(i) || price.get(i) > budgetMax) {
				RESULT = RESULT && false;
				log.printMessage("Accommodation: " + accs.getAccommodationAt(i).getName() 
								+ " has price: " + accs.getAccommodationAt(i).getPrice() 
								+ " which is not in range from " + budgetMin + " to " + budgetMax);
				break;
			}
		}
		
		return this;
	}

	/**
	 * Verify that the filter option button is highlighted
	 * @param option
	 * @return
	 */
	public SearchResultPage verifyFilterHighlighted(FilterBy option) {

		String observedColor = Utilities.getHexColor(new CustomElement().getCssValue(String.format(filterOption, option.getValue()), "background-color"));
		if (!observedColor.equals(HIGHLIGHT_COLOR)) {
			RESULT = RESULT && false;
			log.printMessage("Observed color " + observedColor + " is not matched to " + HIGHLIGHT_COLOR);
		}
		return this;
	}
	
	/**
	 * Verify that the filter option is not highlighted.
	 * @param option
	 * @return
	 */
	public SearchResultPage verifyFilterNotHighlighted(FilterBy option) {

		String observedColor = Utilities.getHexColor(new CustomElement().getCssValue(String.format(filterOption, option.getValue()), "background-color"));
		if (!observedColor.equals(UNHIGHLIGHTED_COLOR)) {
			RESULT = RESULT && false;
			log.printMessage("Observed color " + observedColor + " is not matched to " + UNHIGHLIGHTED_COLOR);
		}
		return this;
	}

	/**
	 * Select to open an accommodation at new tab
	 * @param i
	 * @return
	 */
	public AccommodationDetailPage selectAccommodation(Accommodations accs, Integer i) {
		
		Integer index = i - 1;
		new CustomElement().scrollElementIntoMiddle(String.format(hotelName, i));
		
		String hotelNamePath = String.format("//*[@data-selenium='hotel-name' and text()=\"%s\"]", accs.getAccommodationAt(index).getName());
		//System.out.println(Accommodations.getAccommodationAt(index).getName());
		CustomElement name = new CustomElement(hotelNamePath);
		name.click();
		
		accs.setSelectedAccommodation(accs.getAccommodationAt(index));
		
		return new AccommodationDetailPage();
	}

	public SearchResultPage viewAccommodationScore(Accommodations accs, Integer i) {
		
		Integer index = i - 1;
		new CustomElement().scrollElementIntoMiddle(String.format(hotelName, i));
		
		String hotelReviewPath = String.format(hotelReviewPane, accs.getAccommodationAt(index).getName());
		CustomElement reviewPane = new CustomElement(hotelReviewPath);
		reviewPane.moveMouse();
		
		return this;
	}

	public Boolean verifyAccommodationReviewPoints() {

		String elementPath = "//*[text()='%s']";
		
		if (!new CustomElement(String.format(elementPath, ReviewPoints.Cleanliness.getValue())).isDisplayed())
			return false;
		if (!new CustomElement(String.format(elementPath, ReviewPoints.Facilities.getValue())).isDisplayed())
			return false;
		if (!new CustomElement(String.format(elementPath, ReviewPoints.Location.getValue())).isDisplayed())
			return false;
		if (!new CustomElement(String.format(elementPath, ReviewPoints.Service.getValue())).isDisplayed())
			return false;
		if (!new CustomElement(String.format(elementPath, ReviewPoints.Value.getValue())).isDisplayed())
			return false;
		
		return true;
	}
}
