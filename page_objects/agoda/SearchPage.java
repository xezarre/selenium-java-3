package agoda;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;

import elements.CustomElement;
import java.text.ParseException;
import utils.Constants.OccupancyType;

public class SearchPage {

	private CustomElement txtDestination = new CustomElement(
			"//input[@class='SearchBoxTextEditor SearchBoxTextEditor--autocomplete']");

	private CustomElement lblFromDateTitle = new CustomElement(
			"//div[contains(@class,'IconBox IconBox--checkIn')]//div[@class='SearchBoxTextDescription__title']");

	private CustomElement icoNavNext = new CustomElement(
			"//span[@class='DayPicker-NavButton DayPicker-NavButton--next  ficon ficon-18 ficon-edge-arrow-right']");
	private CustomElement icoNavPrev = new CustomElement("//*[@data-selenium='calendar-previous-month-button']");

	private CustomElement btnSearchRooms = new CustomElement(
			"//button[@class='btn Searchbox__searchButton Searchbox__searchButton--active']");
	private CustomElement icoFromDate = new CustomElement("//div[@class='IconBox IconBox--checkIn']");

	// Dynamic XPath
	private final String xpathSuggestionDestination = "//div[@class='Popup Autocomplete']//li[@class='Suggestion Suggestion__categoryName' and @data-text='%s']";
	private final String xpathDate = "//div[contains(@class,'DayPicker-Day') and contains(@aria-label,'%s')]";
	private final String xpathLblQuantity = "//span[@data-component='desktop-occ-%s-value']";
	private final String xpathIcoMinus = "//span[contains(@data-element-name,'occupancy-selector-panel-%s') and @data-selenium='minus']";
	private final String xpathIcoPlus = "//span[contains(@data-element-name,'occupancy-selector-panel-%s') and @data-selenium='plus']";
	private final String xpathLnkTravellerType = "//*[text()='%s']";

	// Methods Section
	/**
	 * @author Thuy Tran
	 * @return The xpath for traveller types: Solo traveler, Couple/Pair, Family
	 *         travelers, Group travelers, Business travelers
	 */
	protected CustomElement getLnkTravellerType(String travelerType) {
		return new CustomElement(String.format(xpathLnkTravellerType, travelerType));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath based on the date on date picker
	 */
	protected CustomElement getLnkDate(String date) {
		return new CustomElement(String.format(xpathDate, date));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath of Room, Adult and Children quantity
	 */
	protected CustomElement getLblQuantity(OccupancyType type) {
		return new CustomElement(String.format(xpathLblQuantity, type));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath of Minus Room, Adult and Children quantity
	 */
	protected CustomElement getIcoMinus(OccupancyType type) {
		return new CustomElement(String.format(xpathIcoMinus, type));
	}

	/**
	 * @author Thuy Tran
	 * @return The xpath of Minus Room, Adult and Children quantity
	 */
	protected CustomElement getIcoPlus(OccupancyType type) {
		return new CustomElement(String.format(xpathIcoPlus, type));
	}

	/**
	 * @author Thuy Tran
	 * @description Search Hotel
	 */
	public void SearchHotel(String destination, String fromDate, int addDate, String travelerType, int numberRooms,
			int numberAdult, int numberChild) {
		String format = "MMM dd yyyy";
		LocalDate nextDay = getNextDayOfWeek(fromDate);
		String selectedDate = formatDate(nextDay, format);
		String toDate = addDate(selectedDate, addDate, format);
		// Select destination
		searchDestination(destination);
		// Select started date
		selectDate(selectedDate);

		// Select end date
		selectDate(toDate);
		selectTravelerType(travelerType);
		if (numberRooms != 0) {
			selectQuantity(OccupancyType.room, numberRooms);
		}
		if (numberAdult != 0) {
			selectQuantity(OccupancyType.adult, numberAdult);
		}

		if (numberChild != 0) {
			selectQuantity(OccupancyType.children, numberChild);
		}
		btnSearchRooms.click();
	}

	/**
	 * @author Thuy Tran
	 * @description Select traveler type
	 */
	public void selectTravelerType(String travelerType) {
		getLnkTravellerType(travelerType).click();
	}

	/**
	 * @author Thuy Tran
	 * @description Select Room, Adult, Children quantity
	 */
	public void selectQuantity(OccupancyType type, int qty) {
		int actualQty = Integer.parseInt(getLblQuantity(type).getText());
		int difference = qty - actualQty;
		if (difference > 0) {
			while (difference > 0) {
				getIcoPlus(type).click();
				difference = difference - 1;
			}
		} else if (difference < 0) {
			while (difference < 0) {
				getIcoMinus(type).click();
				difference = difference + 1;
			}
		}
	}

	/**
	 * @author Thuy Tran
	 * @description Search Destination
	 */
	public void searchDestination(String destination) {
		txtDestination.click();
		txtDestination.enter(destination);
		// Select the destination in the suggestion list
		new CustomElement(xpathSuggestionDestination, destination).click();
	}

	/**
	 * @author Thuy Tran
	 * @throws java.text.ParseException
	 * @description Search fromDate (checkin date)
	 */
	public void selectDate(String selectedDate) {
		String currentDate = "", futureDate = "";
		String currentSelectedDate = lblFromDateTitle.getText();
		System.out.println("currentSelectedDate: " + currentSelectedDate);
		Date date1 = new Date(), date2 = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");

		DateFormat originalFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
		DateFormat originalFormat_1 = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
		DateFormat targetFormat = new SimpleDateFormat("MMM dd yyyy");
		Date date_current, date_future;
		try {
			date_current = originalFormat.parse(currentSelectedDate);
			currentDate = targetFormat.format(date_current);
			System.out.println("currentDate after format: " + currentDate);
			date_future = originalFormat_1.parse(selectedDate);
			futureDate = targetFormat.format(date_future);
			System.out.println("futureDate after format: " + futureDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			date1 = sdf.parse(currentDate);
			date2 = sdf.parse(futureDate);
			System.out.println("is larger: " + (date1.compareTo(date2) > 0));

			icoFromDate.waitForElementsPresence(By.xpath("//div[@class='DayPicker']"));
			CustomElement expectedDate = getLnkDate(selectedDate);
			Boolean isDisplayed = expectedDate.isDisplayed();
			System.out.println("is date existed: " + isDisplayed);
			int i = 12;
			System.out.println("is larger: " + (date1.compareTo(date2) > 0));

			if (date1.compareTo(date2) > 0) {

				while (isDisplayed != true && i > 0) {
					icoNavPrev.click();
					isDisplayed = expectedDate.isDisplayed();
					i = i - 1;
				}
			}

			else if (date1.compareTo(date2) < 0) {
				while (isDisplayed != true && i > 0) {
					icoNavNext.click();
					isDisplayed = expectedDate.isDisplayed();
					i = i - 1;
				}
			}

			expectedDate.waitForClickable();
			expectedDate.click();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author Thuy Tran
	 * @description getNextDayOfWeek
	 */
	public LocalDate getNextDayOfWeek(String day) {
		LocalDate dt = LocalDate.now();
		if (day == "MONDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		else if (day == "TUESDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
		else if (day == "WEDNESDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY));
		else if (day == "THURSDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
		else if (day == "FRIDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
		else if (day == "SATURDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
		else if (day == "SUNDAY")
			return dt.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));

		return dt;

	}

	/**
	 * @author Thuy Tran
	 * @description Add Date
	 */
	public String addDate(String fromDate, int addDate, String dateFormat) {
		String dateAfterAdd = "";
		SimpleDateFormat simpleFormat = new SimpleDateFormat(dateFormat);

		DateFormat df = new java.text.SimpleDateFormat(dateFormat);
		java.util.Calendar calendar = java.util.Calendar.getInstance();

		try {
			calendar.setTime(df.parse(fromDate));
			calendar.add(java.util.Calendar.DAY_OF_MONTH, +addDate);
			dateAfterAdd = simpleFormat.format(calendar.getTime());

		} catch (Exception ex) {
			System.out.println("Error: " + ex.toString());
		}
		return dateAfterAdd;
	}

	/**
	 * @author Thuy Tran
	 * @description Format Date
	 */
	public String formatDate(LocalDate date, String format) {
		String formattedDate = date.format(DateTimeFormatter.ofPattern(format));
		return formattedDate;
	}

	/**
	 * @author Thuy Tran
	 * @description Format Date
	 */
	public String formatDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		String dateAfterFormat = dateFormat.format(date);
		return dateAfterFormat;
	}

	/**
	 * @author Thuy Tran
	 * @description Get Date
	 */
	public static int getDate(Calendar cal) {
		return cal.get(Calendar.DATE);
	}

	/**
	 * @author Thuy Tran
	 * @description Get Year
	 */
	public static int getYear(Calendar cal) {
		return cal.get(Calendar.YEAR);
	}

	/**
	 * @author Thuy Tran
	 * @description Get Month
	 */
	public static String getMonth(Calendar cal) {
		return Integer.toString(cal.get(Calendar.MONTH) + 1);
	}
}