package agoda;

import driver.DriverManager;
import elements.CustomElement;

public class AccommodationDetailPage extends GeneralPage {
	
	//private Accommodation accommodation;
	
	private String amenityOption = "//*[@data-element-name='abouthotel-amenities-facilities']//span[text()='%s']";

	private String nameHeader = "//*[@data-selenium='hotel-header-name']";
	private String addressHeader = "//*[@data-selenium='hotel-address-map']";
	//private String scoreHeader = "//*[@data-selenium='hotel-header-review-rating']//*[@data-selenium='hotel-header-review-score']";
	
	public AccommodationDetailPage() {
		DriverManager.getDriver().switchTo(handler.get(1));
	}
	
	public Boolean verifyAccommodationDetail() {
		
		Boolean result = true;
		
		new CustomElement().scrollElementIntoMiddle(nameHeader);
		
		String displayedName = new CustomElement(nameHeader).getText();
		String displayedAddress = new CustomElement(addressHeader).getText();
		//String displayedScore = new CustomElement(scoreHeader).getText();
		
		Accommodation myAc = Accommodations.getSelectedAccommodation();
		//System.out.println("myAc: \n" + myAc.toString());
		
		if (!displayedName.equals(myAc.getName())) {
			System.out.println(displayedName + ":" + myAc.getName());
			result = false;
		}
		if (!displayedAddress.contains(myAc.getLocation())) {
			System.out.println(displayedAddress + ":" + myAc.getLocation());
			result = false;
		}
		/*if (!displayedScore.equals(myAc.getReviewScore())) {
			System.out.println(displayedScore + ":" + myAc.getReviewScore());
			result = false;
		}*/
		
		if (Amenities.listSize != 0) {
			
			CustomElement option;
			for (int i = 0; i < Amenities.listSize; i++) {
				option = new CustomElement(String.format(amenityOption, Amenities.getOptionAt(i)));
				System.out.println(String.format(amenityOption, Amenities.getOptionAt(i)));
				if (!option.isDisplayed()) {
					System.out.println(" not display!");
					result = false;
				}
			}
		}
		return result;
	}

	public SearchResultPage backToSearchResultPage() {
		super.closeTab();
		return new SearchResultPage();
	}
}
