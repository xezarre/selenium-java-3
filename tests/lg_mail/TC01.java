package lg_mail;

import org.testng.Assert;
import org.testng.annotations.Test;
import driver.DriverManager;
import lg_email.Email;
import utils.*;

public class TC01 extends BaseTest_Thuy{

	@Test
	/**
	* @author: Thuy Tran	
	*/
	public void TC001() {
		
		LoginPage_Thuy loginPage = new LoginPage_Thuy();
		GeneralPage_Thuy generalPage = new GeneralPage_Thuy();
		ComposeEmailPage composeEmailPage = new ComposeEmailPage();
		String emailSubject = "Test Subject " + Utilities.randomInt(100, 10000);
		Email email = new Email("Thuy Tran","",emailSubject,"Compose and save draft email","attachedFile.txt","");
		
		logger.printMessage("TC01: Compose and save draft email ");		
		
		logger.printMessage("Step 1: Navigate to https://sgmail.logigear.com/");
		logger.printMessage("Step 2: Login with your account");		
		loginPage.login(Constants.email_USERNAME,Constants.email_PASSWORD);
		String winHandleBefore = DriverManager.getDriver().getWindowHandle();
		
		logger.printMessage("Step 3: Compose new email with attachment");
		generalPage.openNewEmailWindow();			
		composeEmailPage.composeEmail(email);
		
		logger.printMessage("Step 4: Save the email and close the composing email popup");
		composeEmailPage.saveEmail();
		DriverManager.getDriver().switchTo().window(winHandleBefore);	
		generalPage.openDraftPage();
		
		logger.printMessage("VP: The email is saved to Draft folder successfully with correct info");
		generalPage.selectEmail(emailSubject);
		Email actualEmail = generalPage.getEmailDetailInfo();
		Assert.assertEquals(email.getToEmail(), actualEmail.getToEmail(), "The To email info is not correct");
		Assert.assertEquals(email.getEmailContent(), actualEmail.getEmailContent(), "The email content info is not correct");
		Assert.assertEquals(email.getEmailSubject(), actualEmail.getEmailSubject(), "The emaill subject info is not correct");
		Assert.assertEquals(email.getFile(), actualEmail.getFile(), "The attachement is not correct");
	}	
}