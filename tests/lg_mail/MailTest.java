package lg_mail;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.Test;

import lg_email.EmailAccount;
import lg_email.EmailMessage;
import utils.ImageHelper;
import utils.Utilities;

public class MailTest extends BaseTest {
	
	@Test 
	public void TC001() {
		
		logger.printMessage("TC01 - Compose and save draft email successfully");
		
		/**
		* @author: Vinh Nguyen	
		* @step steps
		*/
		
		EmailAccount account = EmailAccount.loadFromJSON("emailAccount");
		EmailMessage message = EmailMessage.loadFromJSON("emailMessage").randomizeSubject();
		
		MainPage mainPage = new LoginPage().login(account)
					   .createNewMessage()
					   .composeWithAttachment(message)
					   .save()
					   .openDraftFolder();
					   
		EmailMessage draftMessage = mainPage.selectDraftMail(message.getSubject());
		
		Assert.assertEquals(mainPage.validateDraftMessage(message, draftMessage), Boolean.TRUE);
	}
	
	@Test 
	public void TC002() {
		
		logger.printMessage("TC02 - Compose and send email successfully");
		
		/**
		* @author: Vinh Nguyen	
		* @step steps
		*/
		
		EmailAccount account = EmailAccount.loadFromJSON("emailAccount");
		EmailMessage message = EmailMessage.loadFromJSON("emailMessage").randomizeSubject();
		
		MainPage mainPage = new LoginPage().login(account)
					   .createNewMessage()
					   .composeWithPicture(message)
					   .send()
					   .openInboxFolder();
		
		EmailMessage receivedMessage = mainPage.selectNewMail(message.getSubject());
		
		Assert.assertEquals(mainPage.validateReceivedMesage(message, receivedMessage), Boolean.TRUE);
		
		String downloadedImageFilePath = mainPage.downloadImage().getAbsolutePath();
		String sourceImageFilePath = Utilities.getProjectPath() + "\\test_data\\lg_mail\\" + "picture.jpg";
		
		Assert.assertEquals(ImageHelper.isSimilar(sourceImageFilePath, downloadedImageFilePath), Boolean.TRUE);
		
		mainPage.cleanUp(receivedMessage, new File(downloadedImageFilePath));
		
	}
}
