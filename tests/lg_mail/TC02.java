package lg_mail;

import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.Assert;
import org.testng.annotations.Test;

import driver.DriverManager;
import lg_email.Email;
import utils.Constants;
import utils.Utilities;


public class TC02 extends BaseTest_Thuy{

	@Test
	/**
	* @author: Thuy Tran	
	*/
	public void TC002() throws MalformedURLException, IOException {
		
		LoginPage_Thuy loginPage = new LoginPage_Thuy();
		GeneralPage_Thuy generalPage = new GeneralPage_Thuy();
		ComposeEmailPage composeEmailPage = new ComposeEmailPage();
		String emailSubject = "Test Subject " + Utilities.randomInt(100, 10000);
		Email email = new Email("Thuy Tran","",emailSubject,"Compose and save draft email","","picture.jpg");
		String image = "SavedImage.png";
		
		logger.printMessage("TC02: Compose and send email successfully");		
		
		logger.printMessage("Step 1: Navigate to https://sgmail.logigear.com/");
		logger.printMessage("Step 2: Login with your account");		
		loginPage.login(Constants.email_USERNAME,Constants.email_PASSWORD);
		String winHandleBefore = DriverManager.getDriver().getWindowHandle();
		
		logger.printMessage("Step 3: Compose new email with image inserted in content");
		generalPage.openNewEmailWindow();			
		composeEmailPage.composeEmail(email);
		
		logger.printMessage("Step 4: Send the email to your email");
		composeEmailPage.sendEmail();
		
		logger.printMessage("Step 5: Open the new received email and download the image");	
		DriverManager.getDriver().switchTo().window(winHandleBefore);	
		generalPage.openInboxPage();
		generalPage.selectEmail(email.getEmailSubject());
		generalPage.downloadImage(image);
		
		logger.printMessage("VP: The email is sent successfully with correct info");
		Boolean doesFileExist = generalPage.isFileInFolder(image);

		logger.printMessage("doesFileExist: " + doesFileExist);
		logger.printMessage("VP: Download the image successfully");	
		Email actualEmail = generalPage.getEmailDetailInfo();
		Assert.assertEquals(email.getToEmail(), actualEmail.getToEmail(), "The To email info is not correct");
		Assert.assertEquals(email.getEmailContent(), actualEmail.getEmailContent(), "The email content info is not correct");
		Assert.assertEquals(email.getEmailSubject(), actualEmail.getEmailSubject(), "The emaill subject info is not correct");
		
	}	
}