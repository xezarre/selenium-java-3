package lg_mail;

import org.testng.annotations.BeforeMethod;

import driver.DriverManager;
import utils.Constants.Browser;
import utils.Logger;

public class BaseTest_Thuy {

	public Logger logger = new Logger();

	@BeforeMethod
	public void beforeMethod() {
		DriverManager.initBrowser(Browser.CHROME);
		DriverManager.getDriver().maximize();
		DriverManager.getDriver().navigateToURL("https://sgmail.logigear.com/");
	}

	/*public void afterMethod() {
		DriverManager.getDriver().quit();
	}*/

}
