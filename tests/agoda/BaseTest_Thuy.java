package agoda;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import driver.DriverManager;
import utils.Logger;
import utils.Constants.Browser;

public class BaseTest_Thuy {

	public Logger logger = new Logger();

	@BeforeMethod
	public void beforeMethod() {
		DriverManager.initBrowser(Browser.CHROME);
		DriverManager.getDriver().maximize();
		DriverManager.getDriver().navigateToURL("https://www.agoda.com/");
	}

	@AfterMethod
	public void afterMethod() {
		DriverManager.getDriver().quit();
	}

}