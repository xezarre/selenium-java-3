package agoda;

import static org.testng.Assert.assertTrue;
import java.util.List;
import org.testng.annotations.Test;
import utils.Constants.TravelerType;

 public class TC01 extends BaseTest_Thuy {
 
 	@Test
 	public void TC001() {
		logger.printMessage("TC01: Search and sort hotel successfuly");
 		
		SearchPage searchPage = new SearchPage();
		SearchResultPage_Thuy searchResultPage = new SearchResultPage_Thuy();
		String destination = "Da Nang";
 		
		logger.printMessage("Step 1: Navigate to https://www.agoda.com");
		logger.printMessage("Step 2: Search the hotel");
		
		searchPage.SearchHotel(destination, "FRIDAY", 3, TravelerType.FAMILY.toString(), 2, 4, 0);
		Hotel result1 = searchResultPage.getHotelResultInfo(1);
		Hotel result2 = searchResultPage.getHotelResultInfo(2);
		Hotel result3 = searchResultPage.getHotelResultInfo(3);
		Hotel result4 = searchResultPage.getHotelResultInfo(4);
		Hotel result5 = searchResultPage.getHotelResultInfo(5);
 		
		String actualDestination1 = result1.getDestination();
		String actualDestination2 = result2.getDestination();
		String actualDestination3 = result3.getDestination();
		String actualDestination4 = result4.getDestination();
		String actualDestination5 = result5.getDestination();
 		
		logger.printMessage("VP: Search result displays correctly with first 5 hotels with correct destination");
		assertTrue(actualDestination1.contains(destination), "The destination is not correct");
		assertTrue(actualDestination2.contains(destination), "The destination is not correct");
		assertTrue(actualDestination3.contains(destination), "The destination is not correct");
		assertTrue(actualDestination4.contains(destination), "The destination is not correct");
		assertTrue(actualDestination5.contains(destination), "The destination is not correct");

		logger.printMessage("Step 3: Sort hotels with lowest price");
		searchResultPage.sortByCheapestPrice();
 		
		logger.printMessage("VP: 5 first hotels are sorted with correct order");
		List<String>  actualList = searchResultPage.getPriceOfMultipleHotels(5);
		logger.printMessage("actualListBefore: " + actualList);
		
		Boolean isSorted = searchResultPage.isSortByCheapestPriceCorrectly(actualList);
		assertTrue(isSorted, "The hotel is not sorted correctly");

		logger.printMessage("VP: Search result displays correctly with first 5 hotels with correct destination");
		Hotel result1_AfterSort = searchResultPage.getHotelResultInfo(1);
		Hotel result2_AfterSort = searchResultPage.getHotelResultInfo(2);
		Hotel result3_AfterSort = searchResultPage.getHotelResultInfo(3);
		Hotel result4_AfterSort = searchResultPage.getHotelResultInfo(4);
		Hotel result5_AfterSort = searchResultPage.getHotelResultInfo(5);
 		
		String actualDestination1_AfterSort = result1_AfterSort.getDestination();
		String actualDestination2_AfterSort = result2_AfterSort.getDestination();
		String actualDestination3_AfterSort = result3_AfterSort.getDestination();
		String actualDestination4_AfterSort = result4_AfterSort.getDestination();
		String actualDestination5_AfterSort = result5_AfterSort.getDestination();
 		
		assertTrue(actualDestination1_AfterSort.contains(destination), "The destination is not correct");
		assertTrue(actualDestination2_AfterSort.contains(destination), "The destination is not correct");
		assertTrue(actualDestination3_AfterSort.contains(destination), "The destination is not correct");
		assertTrue(actualDestination4_AfterSort.contains(destination), "The destination is not correct");
		assertTrue(actualDestination5_AfterSort.contains(destination), "The destination is not correct");
 		
 	}
 }