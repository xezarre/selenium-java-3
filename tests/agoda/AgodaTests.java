package agoda;

import org.testng.Assert;
import org.testng.annotations.Test;
import agoda.SearchResultPageEnums.*;

public class AgodaTests extends BaseTest{

	@Test 
	public void TC001() {
		
		logger.printMessage("TEST CASE 001");
		
		/**
		* @author: Vinh Nguyen	
		* @step Navigate to https://www.agoda.com/
		* @step 
		*/
		
		SearchCriteria criteria = SearchCriteria.loadFromJSON("searchCriteria").setDates();
		
		SearchResultPage sr = new HomePage().search(criteria)
					.sort(criteria.getSortOption());
		
		Accommodations accs = sr.getAccommodationsOf(5);
					
				  sr.verifyAccommodateLocation(accs, criteria.getDestination())
					.verifyPriceInOrder(accs, OrderBy.ASC);
		
		Assert.assertEquals(sr.RESULT, Boolean.TRUE);
	}
	
	
	@Test 
	public void TC002() { 
		
		logger.printMessage("TEST CASE 002");
		
		SearchCriteria criteria = SearchCriteria.loadFromJSON("searchCriteria").setDates();
		
		SearchResultPage sr = new HomePage().search(criteria)
					.filterByPrice(criteria.getBudgetMax(), criteria.getBudgetMin())
					.filterByRating(criteria.getRating());
		
		Accommodations accs = sr.getAccommodationsOf(5);
		
				  sr.verifyPriceInRange(accs, criteria.getBudgetMin(), criteria.getBudgetMax())
					.verifyAccommodationRatingFrom(accs, Rating.Three)
					.verifyAccommodateLocation(accs, criteria.getDestination())
					.verifyFilterHighlighted(FilterBy.BUDGET)
					.verifyFilterHighlighted(FilterBy.STAR);
		
		Assert.assertEquals(sr.RESULT, Boolean.TRUE);
		
		sr.clearPriceFilter().verifyFilterNotHighlighted(FilterBy.BUDGET);
		
		Assert.assertEquals(sr.RESULT, Boolean.TRUE);
	}
	
	@Test 
	public void TC003() { 
		
		logger.printMessage("TEST CASE 003");
		
		SearchCriteria criteria = SearchCriteria.loadFromJSON("searchCriteria").setDates();
		
		SearchResultPage sr = new HomePage().search(criteria)
					.filterByPrice(criteria.getBudgetMax(), criteria.getBudgetMin())
					.filterByRating(criteria.getRating())
					.filterByMoreOption(Facilities.NON_SMOKING.getValue());
		
		Accommodations accs = sr.getAccommodationsOf(5);
		
		AccommodationDetailPage ad = sr.selectAccommodation(accs, 5);
		
		Assert.assertEquals(ad.verifyAccommodationDetail(), Boolean.TRUE);
		
				sr = ad.backToSearchResultPage()
					.viewAccommodationScore(accs, 5);
					
		Assert.assertEquals(sr.verifyAccommodationReviewPoints(), Boolean.TRUE);
					
				ad = sr.selectAccommodation(accs, 1);
					
		Assert.assertEquals(ad.verifyAccommodationDetail(), Boolean.TRUE);
		
	}
	 
	
	
}
