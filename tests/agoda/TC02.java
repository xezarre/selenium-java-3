package agoda;

import static org.testng.Assert.assertTrue;
import java.util.List;
import org.testng.annotations.Test;
import utils.Constants.TravelerType;

 public class TC02 extends BaseTest_Thuy {
 
 	@Test
 	public void TC002() {
		logger.printMessage("TC01: Search and filter hotel successfuly");
 		
		SearchPage searchPage = new SearchPage();
		SearchResultPage_Thuy searchResultPage = new SearchResultPage_Thuy();
		String destination = "Da Nang";
		int priceMin = 500000;
		int priceMax = 1000000;
		String star = "3";
 		
		logger.printMessage("Step 1: Navigate to https://www.agoda.com");
		logger.printMessage("Step 2: Search the hotel");
		
		searchPage.SearchHotel(destination, "FRIDAY", 3, TravelerType.FAMILY.toString(), 2, 4, 0);
 		
		logger.printMessage("Step 3: Filter hotel by budget and stars options");
		searchResultPage.filterByBudget(priceMin, priceMax);
		searchResultPage.filterByStar(star);

		logger.printMessage("VP: Search result displays correctly with destination, price and stars");
		Hotel result1 = searchResultPage.getHotelResultInfo(1);
		
		List<String> priceList = searchResultPage.getPriceOfMultipleHotels(5);
		logger.printMessage("priceList for hotel 1: " + priceList.get(0));
		
		assertTrue(result1.getDestination().contains(destination), "The destination is not correct");
		assertTrue(result1.getStar().contentEquals(star + " star"), "The star rating is not correct");
		assertTrue((priceMin < Integer.parseInt(priceList.get(1)) && Integer.parseInt(priceList.get(1)) < priceMax), "The price is not in range");
		
		searchResultPage.clearFilter("Your budget");
		List <String> currentPriceRange = searchResultPage.getCurrentSliderPriceRange();
		List <String> defaultPriceRange = searchResultPage.getDefaultSliderPriceRange();
		
		logger.printMessage("VP: The price slider is reset");
		assertTrue((defaultPriceRange.containsAll(currentPriceRange)), "The price is not reset");
			
 	}
 }