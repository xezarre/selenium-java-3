package BaseTest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import driver.DriverManager;
import utils.Constants.Browser;
import utils.Logger;
import utils.Utilities;

public class BaseTest {

	public Logger logger = new Logger();
	
	@BeforeMethod
	@Parameters({"browser", "application"})
	public void beforeMethod(@Optional("Chrome") String browser, @Optional("[Your application]") String application) {
		DriverManager.initBrowser(Browser.getBrowserName(browser));
		DriverManager.getDriver().maximize();
		DriverManager.getDriver().navigateToURL(Utilities.getUrlOf(application));
	}

	@AfterMethod
	public void afterMethod() {
		DriverManager.getDriver().quit();
	}

}
