package vietjet;

import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import vietjet.Ticket;
import vietjet.VietJetEnum.SeatType;

public class VietJetTests extends BaseTest{

	@Test
	@Parameters("language")
	public void TC001(@Optional("eng") String language) {
		
		HomePage hp = new HomePage();
		Ticket tk = new Ticket(language);
		
		hp.searchTicket(tk);
		
		SelectTravelOptionsPage optionsPage = new SelectTravelOptionsPage();		
		TravelOptions options = new TravelOptions(language);
		
		optionsPage.isPageTitleDisplayCorrectly(options.getTitle());		
		optionsPage.isTicketInfoCorrect(tk);
		
		optionsPage.selectTickets(SeatType.ECO);
		
		optionsPage.isTicketInfoCorrect(tk);
		
		Assert.assertEquals(optionsPage.result, true);				
	}
	
	@Test
	@Parameters("language")
	public void TC002(@Optional("eng") String language) {
		
		HomePage hp = new HomePage();
		Ticket tk = new Ticket(language);
		hp.searchTicket(tk);
		
		SelectFarePage sfpage= new SelectFarePage();
		sfpage.verifyTicketInfoWithoutDate(tk);		
		sfpage.selectCheapestTour(tk);
		SelectTravelOptionsPage optionsPage = new SelectTravelOptionsPage();
		optionsPage.isTicketInfoCorrect(tk);
		TravelOptions options = new TravelOptions(language);
		optionsPage.isPageTitleDisplayCorrectly(options.getTitle());
		optionsPage.selectTickets(SeatType.PROMO);		
		optionsPage.isTicketInfoCorrect(tk);		
		Assert.assertEquals(optionsPage.result, true);				
	}
}