package vietjet;

import utils.Constants;
import utils.ExcelHelper;
import utils.Utilities;

public class TravelOptions {

	private String pageTitle;
	
	private static final String travelOptionsPath = Utilities.getProjectPath() + "\\test_data\\vietjet\\%s";

	public String getTitle() {return pageTitle;}	
	public void setTitle(String title) {this.pageTitle = title;}
	
	public TravelOptions(String filter) {
		ExcelHelper ex = new ExcelHelper();
		Object[][] optionsPage = ex.getDataFromSheet(String.format(Constants.vietjetInfoFilePath,"travelOptions.xlsx"),"pageInfo","language",filter);
		
		this.setTitle(optionsPage[0][1].toString());
	}
	
	public TravelOptions(String fileName, String sheetName, String column, String filter) {
		ExcelHelper ex = new ExcelHelper();
		Object[][] optionsPage = ex.getDataFromSheet(String.format(travelOptionsPath,fileName),sheetName,column,filter);
		
		this.setTitle(optionsPage[0][1].toString());
	}	
}
