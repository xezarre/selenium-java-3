package vietjet;

import java.time.LocalDate;

import utils.DateTimeHelper;
import utils.ExcelHelper;
import utils.Constants;

public class Ticket {

	private LocalDate departDate = null;
	private LocalDate returnDate = null;
	private String flightType;
	private String departFrom;
	private String arriveAt;
	private String currency;
	private String lowestFare, lowestFareLabel;
	private Integer adult;
	private int tripDuration;
	private int monthAmount;
	
	// Get Set methods
	
	public String getFlightType() {return flightType;}	
	public void setFlightType(String flightType) {this.flightType = flightType;}
	
	public String getDepartFrom() {return departFrom;}	
	public void setDepartFrom(String departFrom) {this.departFrom = departFrom;}
	
	public String getReturnFrom() {return arriveAt;}	
	public void setReturnFrom(String arriveAt) {this.arriveAt = arriveAt;}
	
	public Integer getAdult() {return adult;}	
	public void setAdult(Integer adult) {this.adult = adult;}
	
	public String getCurrency() {return currency;}	
	public void setCurrency(String currency) {this.currency = currency;}
	
	public LocalDate getDepartDate() {return this.departDate;}	
	public void setDepartDate(LocalDate departDate) {this.departDate = departDate;}
	
	public LocalDate getReturnDate() {return this.returnDate;}	
	public void setReturnDate(LocalDate returnDate) {this.returnDate = returnDate;}

	public String getLowestFare() {return this.lowestFare;}	
	public void setLowestFare(String lowestFare) {this.lowestFare = lowestFare;}
	
	public String getLowestFareLabel() {return this.lowestFareLabel;}	
	public void setLowestFareLabel(String lowestFareLabel) {this.lowestFareLabel = lowestFareLabel;}
	
	public int getTripDuration() {return this.tripDuration;}	
	public void setTripDuration(int tripDuration) {this.tripDuration = tripDuration;}
	
	public int getMonthAmount() {return this.monthAmount;}	
	public void setMonthAmount(int monthAmount) {this.monthAmount = monthAmount;}
	
	// Constructor	
	
	public Ticket(String filter) {
		ExcelHelper ex = new ExcelHelper();
		Object[][] tk = ex.getDataFromSheet(String.format(Constants.vietjetInfoFilePath, "ticketInfo.xlsx"), "ticketInfo", "language", filter);
		
		this.setFlightType(tk[0][1].toString());
		this.setDepartFrom(tk[0][2].toString());
		this.setReturnFrom(tk[0][3].toString());		
		this.setCurrency(tk[0][4].toString());
		this.setAdult((int)Float.parseFloat(tk[0][5].toString()));
		this.setLowestFare(tk[0][6].toString());
		this.setLowestFareLabel(tk[0][7].toString());
		this.setDepartDate(calculateStartDate(tk[0][8].toString()));
		this.setReturnDate(DateTimeHelper.plusDate(this.getDepartDate(),(int)Float.parseFloat(tk[0][9].toString())));
		this.setTripDuration((int)Float.parseFloat(tk[0][9].toString()));
		this.setMonthAmount((int)Float.parseFloat(tk[0][10].toString()));
	}
	
	public Ticket(String fileName, String sheetName, String column, String filter) {
		ExcelHelper ex = new ExcelHelper();
		Object[][] tk = ex.getDataFromSheet(String.format(Constants.vietjetInfoFilePath, fileName), sheetName, column, filter);
		
		this.setFlightType(tk[0][1].toString());
		this.setDepartFrom(tk[0][2].toString());
		this.setReturnFrom(tk[0][3].toString());
		this.setCurrency(tk[0][4].toString());
		this.setAdult((int)Float.parseFloat(tk[0][5].toString()));		
	}
	
	public Ticket(String fileName, String sheetName, String column, String filter, LocalDate departDate, LocalDate returnDate) {
		ExcelHelper ex = new ExcelHelper();
		Object[][] tk = ex.getDataFromSheet(String.format(Constants.vietjetInfoFilePath, fileName), sheetName, column, filter);
		
		this.setFlightType(tk[0][1].toString());
		this.setDepartFrom(tk[0][2].toString());
		this.setReturnFrom(tk[0][3].toString());
		this.setDepartDate(departDate);
		this.setReturnDate(returnDate);
		this.setCurrency(tk[0][4].toString());
		this.setAdult((int)Float.parseFloat(tk[0][5].toString()));		
	}
	
	private LocalDate calculateStartDate(String targetDate) {
		String targetUnit = targetDate.split(" ")[1].trim();
		int duration = Integer.parseInt(targetDate.split(" ")[0].trim());
		LocalDate returnDate=null;
		if(targetUnit.equals("day")) {
			returnDate = DateTimeHelper.plusDate(DateTimeHelper.getCurrentDate(), duration);
		} else if(targetUnit.equals("month")) {
			returnDate = DateTimeHelper.plusMonth(DateTimeHelper.getCurrentDate(), duration);
		}
		return returnDate;
	}
	
	public String toString() {
		return "From: " + this.getDepartFrom() + "\n"
			 + "To: " + this.getReturnFrom() + "\n"
			 + "Depart Date: " + this.getDepartDate() + "\n"
			 + "Return Date: " + this.getReturnDate() + "\n"
			 + "Flight Type: " + this.getFlightType() + "\n"
			 + "Currency: " + this.getCurrency() + "\n"
			 + "Passenger: " + this.getAdult();
	}
	
	
}
