package vietjet;

public class VietJetEnum {
	public enum DirectionType
    {
        DEPART("Depart"),
        RETURN("Return");
       
		private String _direction;
		
		private DirectionType(String direction) {
    		this._direction = direction;
    	}
   	
    	public String getDirection() {
    		return this._direction;
    	}
    	
    	public String getShortcutDirection() {
    		return this._direction.substring(0,3);
    	}
    }
	
	public enum SeatType
    {
        ECO("Eco"),
        PROMO("Promo"),
        SKYBOSS("Skyboss");
       
		private String _type;
		
		private SeatType(String type) {
    		this._type = type;
    	}
   	
    	public String getSeatType() {
    		return this._type;
    	}    	
    }
}
