package settings;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class oSettings {
	
	@JsonProperty("longTimeout") private Integer longTimeout;
	@JsonProperty("shortTimeout") private Integer shortTimeout;
	@JsonProperty("hubUrl") private String hubUrl;
	@JsonProperty("agodaUrl") private String agodaUrl;
	@JsonProperty("vietjetUrl") private String vietjetUrl;
	@JsonProperty("lgmailUrl") private String lgmailUrl;
	
	private static final String jsonFilePath = System.getProperty("user.dir") + "\\test_data\\settings\\%s";
	private static final String jsonFileName = "settings.json";
	
	public oSettings() {}
	
	public void setLongTimeout(Integer longTimeout) { this.longTimeout = longTimeout; }
	public void setShortTimeout(Integer shortTimeout) { this.shortTimeout = shortTimeout; }
	public void setHubUrl(String hubUrl) { this.hubUrl = hubUrl; }
	public void setAgodaUrl(String agodaUrl) { this.agodaUrl = agodaUrl; }
	public void setVietjetUrl(String vietjetUrl) { this.vietjetUrl = vietjetUrl; }
	public void setLgmailUrl(String lgmailUrl) { this.lgmailUrl = lgmailUrl; }

	public Integer getLongTimeout() { return this.longTimeout; }
	public Integer getShortTimeout() { return this.shortTimeout; }
	public String getHubUrl() { return this.hubUrl; }
	public String getAgodaUrl() { return this.agodaUrl; }
	public String getVietjetUrl() { return this.vietjetUrl; }
	public String getLgmailUrl() { return this.lgmailUrl; }
	
	/**
	 * Load JSON file then map to oSetting object
	 * @author Vinh
	 * @return
	 */
	public static oSettings loadSettings()
	{
		ObjectMapper mapper = new ObjectMapper();
		oSettings obj = null;
		try {
			obj = mapper.readValue(new File(String.format(jsonFilePath, jsonFileName)), oSettings.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	
	public String toString() {
		return "Long timeout: " + this.longTimeout + "\n" +
			   "Short timeout: " + this.shortTimeout + "\n" +
			   "Hub Url: " + this.hubUrl + "\n" +
			   "Agoda Url: " + this.agodaUrl + "\n" +
			   "Vietjet Url: " + this.vietjetUrl + "\n" +
			   "LG Mail Url: " + this.lgmailUrl + "\n";
	}
}
