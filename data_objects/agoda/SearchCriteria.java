package agoda;

import java.io.File;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import utils.Utilities;

public class SearchCriteria {
	
	@JsonProperty("category") private String category;
	@JsonProperty("destination") private String destination;
	@JsonProperty("tripDuration") private Integer tripDuration;
	@JsonProperty("startDate") private String startDate;
	@JsonProperty("endDate") private String endDate;
	@JsonProperty("travelType") private String travelType;
	@JsonProperty("room") private Integer room;
	@JsonProperty("adult") private Integer adult;
	@JsonProperty("child") private Integer child;
	@JsonProperty("sortOption") private String sortOption;
	@JsonProperty("budgetMin") private Integer budgetMin;
	@JsonProperty("budgetMax") private Integer budgetMax;
	@JsonProperty("rating") private String rating;
	
	private static final String jsonFilePath = System.getProperty("user.dir") + "\\test_data\\agoda\\%s";
	//private static final DateTimeFormatter ISO = DateTimeFormatter.ISO_LOCAL_TIME;
	private static final DateTimeFormatter EMDY = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");
	
	/**
	 * Constructor 1
	 * @param category
	 * @param destination
	 * @param startDate
	 * @param endDate
	 * @param tripDuration
	 * @param travelType
	 * @param room
	 * @param adult
	 * @param child
	 */
	public SearchCriteria(String category, String destination, String startDate, String endDate, 
						Integer tripDuration, String travelType, Integer room, Integer adult, Integer child,
						String sortOption, Integer budgetMin, Integer budgetMax, String rating) {
		this.category = category;
		this.destination = destination;
		this.startDate = startDate;
		this.endDate = endDate;
		this.tripDuration = tripDuration;
		this.travelType = travelType;
		this.room = room;
		this.adult = adult;
		this.child = child;
		this.sortOption = sortOption;
		this.budgetMin = budgetMin;
		this.budgetMax = budgetMax;
		this.rating = rating;
	}
	
	/**
	 * Constructor 2
	 */
	public SearchCriteria() {
		
	}

	public String getCategory() { return category; }
	public String getDestination() { return destination; }
	public String getStartDate() { return startDate; }
	public String getEndDate() { return endDate; }
	public Integer getTripDuration() { return tripDuration; }
	public String getTravelType() { return travelType; }
	public Integer getRoom() { return room; }
	public Integer getAdult() { return adult; }
	public Integer getChild() { return child; }
	public String getSortOption() { return sortOption; }
	public Integer getBudgetMin() { return budgetMin; }
	public Integer getBudgetMax() { return budgetMax; }
	public String getRating() { return rating; }
	
	public void setCategory(String category) { this.category = category; }
	public void setDestination(String destination) { this.destination = destination; }
	public void setStartDate(String startDate) { this.startDate = startDate; }
	public void setEndDate(String endDate) { this.endDate = endDate; }
	public void setTripDuration(Integer tripDuration) { this.tripDuration = tripDuration; }
	public void setTravelType(String travelType) { this.travelType = travelType;}
	public void setRoom(Integer room) { this.room = room; }
	public void setAdult(Integer adult) { this.adult = adult; }
	public void setChild(Integer child) { this.child = child; }
	public void setSortOption(String sortOption) { this.sortOption = sortOption; }
	public void setBudgetMin(Integer budgetMin) { this.budgetMin = budgetMin; }
	public void setBudgetMax(Integer budgetMax) { this.budgetMax = budgetMax; }
	public void setRating(String rating) { this.rating = rating; }

	public SearchCriteria setDates() {
		
		String statement = this.startDate;
		
		this.startDate = calculateStartDate(statement);
		this.endDate = calculateEndDate(statement, this.getTripDuration());
		return this;
	}
	
	
	/**
	 * Get Start date of the trip
	 * @author Vinh
	 * @return
	 */
	private static String calculateStartDate(String dateStatement) {
		return LocalDate.now().with(getAdjuster(dateStatement)).format(EMDY).toString();
	}
	
	/**
	 * Get End date of the trip.
	 * @author Vinh
	 * @return 
	 */
	private static String calculateEndDate(String startDateStatement, Integer duration) {
		return LocalDate.now().with(getAdjuster(startDateStatement)).plusDays(duration).format(EMDY).toString();
	}
	
	/**
	 * Parse date statement to TemporalAdjuster
	 * @param statement
	 * @return
	 */
	private static TemporalAdjuster getAdjuster(String statement) {
		
		String[] array = statement.trim().split(" ");
		TemporalAdjuster ta = null;
		
		if (array[0].equals("next")) {
			
			switch(array[1]) {
			case "Monday":
				ta = TemporalAdjusters.next(DayOfWeek.MONDAY);
				break;
			case "Tuesday":
				ta = TemporalAdjusters.next(DayOfWeek.TUESDAY);
				break;
			case "Wednesday":
				ta = TemporalAdjusters.next(DayOfWeek.WEDNESDAY);
				break;
			case "Thursday":
				ta = TemporalAdjusters.next(DayOfWeek.THURSDAY);
				break;
			case "Friday":
				ta = TemporalAdjusters.next(DayOfWeek.FRIDAY);
				break;
			case "Saturday":
				ta = TemporalAdjusters.next(DayOfWeek.SATURDAY);
				break;
			case "Sunday":
				ta = TemporalAdjusters.next(DayOfWeek.SUNDAY);
				break;
			default:
				ta = TemporalAdjusters.next(DayOfWeek.SATURDAY);
				break;
			}
		}
		return ta;
	}
	
	/**
	 * Load JSON file then map to a SearchCriteria
	 * @author Vinh
	 * @return
	 */
	public static SearchCriteria loadFromJSON(String fileName)
	{
		ObjectMapper mapper = new ObjectMapper();
		SearchCriteria obj = null;
		try {
			obj = mapper.readValue(new File(String.format(jsonFilePath, Utilities.validateJSONFileName(fileName))), SearchCriteria.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

}
