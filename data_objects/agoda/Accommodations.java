package agoda;

import java.util.ArrayList;

public class Accommodations {
	
	private static ArrayList<Accommodation> list = new ArrayList<Accommodation>();
	private static Accommodation selectedAccommodation = new Accommodation();
	public Integer listSize = list.size();
	
	public Accommodations() {}
	
	public void addAccommodation(Accommodation accommodation) {
		list.add(accommodation);
	}
	
	public Accommodation getAccommodationAt(Integer index) {
		return list.get(index);
	}
	
	public ArrayList<Accommodation> getList() {
		return list;
	}
	
	public void setSelectedAccommodation(Accommodation accommodation) {
		selectedAccommodation = accommodation;
	}
	
	public static Accommodation getSelectedAccommodation() {
		return selectedAccommodation;
	}
}
