package agoda;

import java.util.ArrayList;

public class Amenities {
	
	private static ArrayList<String> amenities = new ArrayList<String>();
	public static Integer listSize = amenities.size();
	
	public Amenities() {}
	
	public static void addOption(String amenityOption) {
		amenities.add(amenityOption);
	}
	
	public static String getOptionAt(Integer index) {
		return amenities.get(index);
	}
	
	public static ArrayList<String> getList() {
		return amenities;
	}

}
