package agoda;

public class SearchResultPageEnums {

	public enum SortBy {
		POPULAR("search-sort-recommended"), 
		BESTMATCH("search-sort-personalize"), 
		LOWEST_PRICE_FIRST("search-sort-price"),
		DISTANCE("search-sort-distance-landmark"),
		TOP_REVIEWED("search-sort-guest-rating");

		private String sort;
		
		SortBy(String s) {
			this.sort = s; 
		}
		public String getValue() {
			return this.sort;
		}
		
		public static String getSortOptionBy(String name) {
			switch (name.trim()) {
			case "Popular":
				return SortBy.POPULAR.getValue();
			case "Best match":
				return SortBy.BESTMATCH.getValue();
			case "Lowest price":
				return SortBy.LOWEST_PRICE_FIRST.getValue();
			case "Distance":
				return SortBy.DISTANCE.getValue();
			case "Top reviewed":
				return SortBy.TOP_REVIEWED.getValue();
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}
	
	public enum FilterBy {
		POPULAR("search-filter-type-popular"), 
		BUDGET("search-filter-type-pricefilterrange"), 
		STAR("search-filter-type-starrating"),
		LOCATION("search-filter-type-locationfilters"),
		MORE("search-filter-type-more");

		private String filter;
		
		FilterBy(String s) {
			this.filter = s; 
		}
		
		public String getValue() {
			return this.filter;
		}
	}
	
	public enum Rating {
		One(1),
		Two(2), 
		Three(3), 
		Four(4), 
		Five(5);

		private Integer rate;
		
		Rating(Integer s) {
			this.rate = s; 
		}
		
		public Integer getValue() {
			return this.rate;
		}
		
		public static String getRateOptionByName(String rate) {
			switch (rate) {
			case "1 star":
				return Rating.One.getValue() + "";
			case "2 star":
				return Rating.Two.getValue() + "";
			case "3 star":
				return Rating.Three.getValue() + "";
			case "4 star":
				return Rating.Four.getValue() + "";
			case "5 star":
				return Rating.Five.getValue() + "";
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}
	
	public enum Facilities {
		SWIMMING_POOL("Swimming pool"), 
		INTERNET("Internet"),
		CAR_PARK("Car park"),
		AIRPORT_TRANSFER("Airport transfer"), 
		GYM_FITNESS("Gym/fitness"),
		FRONT_DESK("Front desk [24-hour]"),
		CHILD_FRIENDLY("Family/child friendly"),
		NON_SMOKING("Non-smoking"),
		SPA_SAUNA("Spa/sauna"),
		RESTAURANT("Restaurants"),
		SMOKING_AREA("Smoking area"),
		PET_ALLOWED("Pets allowed"),
		NIGHTCLUB("Nightclub"),
		DISABLE_GUEST("Facilities for disabled guests"),
		FOR_BUSINESS("Business facilities");

		private String option;
		
		Facilities(String s) {
			this.option = s; 
		}
		public String getValue() {
			return this.option;
		}
	}
	
	public enum OrderBy {
		ASC("asc"), DESC("desc");
		
		private String order;
		
		OrderBy(String order){
			this.order = order;
		}
		
		public String getValue() {
			return this.order;
		}
	}
	
	public enum ReviewPoints {
		Cleanliness("Cleanliness"), 
		Facilities("Facilities"),
		Location("Location"),
		Service("Service"),
		Value("Value for money");
		
		private String review;
		
		ReviewPoints(String review){
			this.review = review;
		}
		
		public String getValue() {
			return this.review;
		}
	}
		
	
}
