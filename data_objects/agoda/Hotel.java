package agoda;

public class Hotel {
	//Properties
	private String destination;
	private String price;
	private String star;

	//Constructor
	public Hotel(String destination, String price, String star) 
	{
		this.destination = destination;
		this.price = price;
		this.star = star;
	}

	public Hotel() 
	{

	}
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}
	
}