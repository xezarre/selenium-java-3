package agoda;

public class HomePageEnums {
	
	public enum Adult {
		VALUE("desktop-occ-adult-value"), 
		PLUS("plus"), 
		MINUS("minus");

		private String adult;
		
		Adult(String s) {
			this.adult = s; 
		}
		public String getValue() {
			return this.adult;
		}
	}
	
	public enum Room {
		VALUE("desktop-occ-room-value"), 
		PLUS("plus"), 
		MINUS("minus");

		private String room;
		
		Room(String s) {
			this.room = s; 
		}
		public String getValue() {
			return this.room;
		}
	}
	
	public enum Services {
		HOTELS_AND_HOMES("allRoomsTab"), 
		FLIGHT_AND_HOTEL("agodaPackagesTab"), 
		FLIGHTS("agodaFlightsTab"), 
		TRANSPORTS("agodaGroundTransportTab");

		private String service;
		
		Services(String s) {
			this.service = s; 
		}
		public String getValue() {
			return this.service;
		}
		
		public static Services getServicesByName(String name) {
			switch (name.trim()) {
			case "Room":
				return Services.HOTELS_AND_HOMES;
			case "Package":
				return Services.FLIGHT_AND_HOTEL;
			case "Flight":
				return Services.FLIGHTS;
			case "Transport":
				return Services.TRANSPORTS;
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}
	
	public enum Months {
		Jan(1), 
		Feb(2), 
		Mar(3), 
		Apr(4),
		May(5),
		Jun(6),
		Jul(7),
		Aug(8),
		Sep(9),
		Oct(10),
		Nov(11),
		Dec(12);

		private Integer month;
		
		Months(int i) {
			this.month = i; 
		}
		public Integer getValue() {
			return this.month;
		}
		
		public static Months getMonthByName(String name) {
			switch (name.trim()) {
			case "Jan":
				return Months.Jan;
			case "Feb":
				return Months.Feb;
			case "Mar":
				return Months.Mar;
			case "Apr":
				return Months.Apr;
			case "May":
				return Months.May;
			case "Jun":
				return Months.Jun;
			case "Jul":
				return Months.Jul;
			case "Aug":
				return Months.Aug;
			case "Sep":
				return Months.Sep;
			case "Oct":
				return Months.Oct;
			case "Nov":
				return Months.Nov;
			case "Dec":
				return Months.Dec;
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}
	
	public enum TravelTypes {
		SOLO("traveler-solo"), 
		COUPLE("traveler-couples"), 
		FAMILY("traveler-families"), 
		GROUP("traveler-group"),
		BUSINESS("traveler-business");

		private String type;
		
		TravelTypes(String s) {
			this.type = s; 
		}
		public String getValue() {
			return this.type;
		}
		
		public static TravelTypes getTravelTypeByName(String name) {
			switch (name.trim()) {
			case "Solo":
				return TravelTypes.SOLO;
			case "Couple":
				return TravelTypes.COUPLE;
			case "Family":
				return TravelTypes.FAMILY;
			case "Group":
				return TravelTypes.GROUP;
			case "Business":
				return TravelTypes.BUSINESS;
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}

}
