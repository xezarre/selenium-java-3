package agoda;

public class Accommodation {
	
	private String name;
	private String location;
	private String rate;
	private String reviewScore;
	private String price;
	
	public Accommodation() {}
	
	public Accommodation(String name, String location, String rate, String price, String reviewScore) {
		this.name = name;
		this.location = location;
		this.rate = rate;
		this.reviewScore = reviewScore;
		this.price = price;
	}
	
	public void setName(String name) { this.name = name; }
	public void setLocation(String location) { this.location = location; }
	public void setRate(String rate) { this.rate = rate; }
	public void setReviewScore(String score) { this.reviewScore = score; }
	public void setPrice(String price) { this.price = price; }
	
	public String getName() { return name; }
	public String getLocation() { return location; }
	public String getRate() { return rate; }
	public String getReviewScore() { return reviewScore; }
	public String getPrice() { return price; }
	
	public String toString() {
		
		return  "Hotel: " + this.name + "\n" +
				"Location: " + this.location + "\n" +
				"Rate: " + this.rate + "\n" + 
				"Score: " + this.reviewScore + "\n" +
				"Price: " + this.price;
	}

}
