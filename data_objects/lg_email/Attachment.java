package lg_email;

public class Attachment {

	private String fileName;
	private String filePath;
	//private String fileSize;
	
	public Attachment() {}
	
	public Attachment(String fileName) {
		this.fileName = fileName;
	}
	
	public Attachment(String fileName, String filePath) {
		this.fileName = fileName;
		this.filePath = filePath;
		//this.fileSize = fileSize;
	}
	
	public void setFileName(String fileName) { this.fileName = fileName; }
	public void setFilePath(String filePath) { this.filePath = filePath; }
	//public void setFileSize(String fileSize) { this.fileSize = fileSize; }
	
	public String getFileName() { return fileName; }
	public String getFilePath() { return filePath; }
	//public String getFileSize() { return fileSize; }
	
}
