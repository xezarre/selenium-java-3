package lg_email;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import utils.Utilities;

public class EmailAccount {
	
	@JsonProperty("username") private String username;
	@JsonProperty("password") private String password;
	
	private static final String jsonFilePath = System.getProperty("user.dir") + "\\test_data\\lg_mail\\%s";
	
	public EmailAccount() {}
	
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }
	
	public String getUsername() { return username; }
	public String getPassword() { return password; }
	
	/**
	 * Load JSON file then map to an email account
	 * @author Vinh
	 * @return
	 */
	public static EmailAccount loadFromJSON(String fileName)
	{
		ObjectMapper mapper = new ObjectMapper();
		EmailAccount obj = null;
		try {
			obj = mapper.readValue(new File(String.format(jsonFilePath, Utilities.validateJSONFileName(fileName))), EmailAccount.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

}
