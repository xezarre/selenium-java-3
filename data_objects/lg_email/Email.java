package lg_email;

public class Email {
	//Properties
		private String toEmail;
		private String ccEmail;
		private String emailSubject;
		private String emailContent;
		private String file;
		private String picture;

		//Constructor
		public Email(String toEmail, String ccEmail, String emailSubject, String emailContent, String file, String picture) 
		{
			this.toEmail = toEmail;
			this.ccEmail = ccEmail;
			this.emailSubject = emailSubject;
			this.emailContent = emailContent;
			this.file = file;
			this.picture = picture;
		}

		public Email() 
		{
	
		}
		public String getToEmail() {
			return toEmail;
		}

		public void setToEmail(String toEmail) {
			this.toEmail = toEmail;
		}
		
		public String getCCEmail() {
			return ccEmail;
		}

		public void setCCEmail(String ccEmail) {
			this.ccEmail = ccEmail;
		}
		
		public String getEmailSubject() {
			return emailSubject;
		}

		public void setEmailSubject(String emailSubject) {
			this.emailSubject = emailSubject;
		}
		
		public String getEmailContent() {
			return emailContent;
		}

		public void setEmailContent(String emailContent) {
			this.emailContent = emailContent;
		}
		
		public String getFile() {
			return file;
		}

		public void setFile(String file) {
			this.file = file;
		}
		
		public String getPicture() {
			return picture;
		}

		public void setPicture(String picture) {
			this.picture = picture;
		}
}
