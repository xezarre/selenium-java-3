package lg_email;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import utils.Utilities;

public class EmailMessage {
	
	@JsonProperty("subject") private String subject;
	@JsonProperty("body") private String body;
	@JsonProperty("receiver") private String receiver;
	@JsonProperty("ccReceiver") private String ccReceiver;
	@JsonProperty("bccReceiver") private String bccReceiver;
	@JsonProperty("attachmentsList") private ArrayList<Attachment> attachmentsList;
	
	private static final String jsonFilePath = System.getProperty("user.dir") + "\\test_data\\lg_mail\\%s";
	
	public EmailMessage() {}
	
	public EmailMessage(String receiver, String ccReceiver, String bccReceiver, String subject, String body) {
		this.receiver = receiver;
		this.ccReceiver = ccReceiver;
		this.bccReceiver = bccReceiver;
		this.subject = subject;
		this.body = body;
	}
	
	public void setReceiver(String receiver) {this.receiver = receiver;}
	public void setCCReceiver(String ccReceiver) {this.ccReceiver= ccReceiver;}
	public void setBCCReceiver(String bccReceiver) {this.bccReceiver = bccReceiver;}
	public void setSubject(String subject) {this.subject = subject;}
	public void setBody(String body) {this.body = body;}
	public void setAttachment(ArrayList<Attachment> attachmentList) {this.attachmentsList = attachmentList;}
	
	public String getReceiver() { return receiver; }
	public String getCCReceiver() { return ccReceiver; }
	public String getBCCReceiver() { return bccReceiver; }
	public String getSubject() { return subject; }
	public String getBody() { return body; }
	public ArrayList<Attachment> getAttachments() { return attachmentsList; }
	
	public ArrayList<Attachment> getImageAttachments(){
		
		ArrayList<Attachment> images = new ArrayList<Attachment>();
	
		this.attachmentsList.forEach((attachment) -> {
			if (Utilities.isImage(new File(Utilities.getProjectPath() + attachment.getFilePath() + attachment.getFileName())))
				images.add(attachment);
		});
		if (images.isEmpty())
			return null;
		
		return images;
	}
	
	public ArrayList<Attachment> getDocumentAttachments() {
		
		ArrayList<Attachment> documents = new ArrayList<Attachment>();
	
		this.attachmentsList.forEach((attachment) -> {
			if (!Utilities.isImage(new File(Utilities.getProjectPath() + attachment.getFilePath() + attachment.getFileName())))
				documents.add(attachment);
		});
		if (documents.isEmpty())
			return null;
		
		return documents;
	}

	/**
	 * Load JSON file then map to an email message
	 * @author Vinh
	 * @return
	 */
	public static EmailMessage loadFromJSON(String fileName)
	{
		ObjectMapper mapper = new ObjectMapper();
		EmailMessage obj = null;
		try {
			/*
			 * TypeFactory typeFactory = mapper.getTypeFactory(); CollectionType
			 * collectionType = typeFactory.constructCollectionType( ArrayList.class,
			 * Attachments.class);
			 */
			obj = mapper.readValue(new File(String.format(jsonFilePath, Utilities.validateJSONFileName(fileName))), EmailMessage.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

	public EmailMessage randomizeSubject() {
		
		this.subject = subject + " " + Utilities.generateRandomString(5);
		return this;
	}
	
	public String toString() {
		return "Receiver: " + this.receiver + "\n"
			 + "Cc: " + this.ccReceiver + "\n"
			 + "Bcc: " + this.bccReceiver + "\n"
			 + "Subject: " + this.subject + "\n"
			 + "Body: " + this.body;
	}
	
	
}
