package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelHelper {
	
	private Sheet sheet = null;
	
	public Sheet getSheetData(String fileName, String sheetName) {
	    try {
		    File file = new File(fileName);

		    FileInputStream inputStream = new FileInputStream(file);
		    Workbook wb = null;
		    
		    String fileExtensionName = fileName.substring(fileName.indexOf("."));

		    if (fileExtensionName.equals(".xlsx")) {
		    	wb = new XSSFWorkbook(inputStream);
		    }
		    else if(fileExtensionName.equals(".xls")) {
		    	wb = new HSSFWorkbook(inputStream);
		    }
		    sheet = wb.getSheet(sheetName);
		    return sheet;    
	    } catch (IOException ioe) {
	    		System.out.println(ioe.getMessage());
	    		return null;
	    }		
	}
	
	public Object[][] getDataFromSheet(String fileName, String sheetName) {
		
		Object[][] object = null;
		Sheet exSheet = this.getSheetData(fileName, sheetName);
	
	    int rowCount = exSheet.getLastRowNum()- exSheet.getFirstRowNum();
	    int colCount = exSheet.getRow(0).getLastCellNum();
	    object = new Object[rowCount][colCount];
	    
	    for (int i = 0; i < rowCount; i++) {	        
	        Row row = exSheet.getRow(i+1);	        
	        for (int j = 0; j < row.getLastCellNum(); j++) {	            
	            object[i][j] = row.getCell(j).toString();
	        }
	    }	    
	    return object;    
	}
	
	public Object[][] getDataFromSheet(String fileName, String sheetName, String columnName, String filter){
		
		Object[][] object = null;
		try {
			Sheet exSheet = this.getSheetData(fileName, sheetName);
		
		    int rowCount = exSheet.getLastRowNum() - exSheet.getFirstRowNum();
		    int colCount = exSheet.getRow(0).getLastCellNum();
		    
		    int filterColumn = getColumnIndexByColumnName(columnName);
		    
		    object = new Object[1][colCount];
		    for (int i = 0; i < rowCount; i++) {	        
		        Row row = exSheet.getRow(i+1);
		        if (row.getCell(filterColumn).toString().equals(filter)) {
		        	for (int j = 0; j < row.getLastCellNum(); j++) {
		        		object[0][j] = row.getCell(j).toString();
		        	}
		        }
		    }
		    return object; 
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	    return object;    
	}
	
	public int getColumnIndexByColumnName(String columnName) {
		
		try {
			int totalHeader = this.sheet.getRow(0).getLastCellNum();
			//System.out.println("Total columns are: "+totalHeader);
			Row row = this.sheet.getRow(0);
			for (int i = 0; i < totalHeader-1; i++) {
				//System.out.println("Current column name '" + row.getCell(i).toString() + "' at index: " + i);
				if(row.getCell(i).toString().equals(columnName)) {
					//System.out.println("Total columns are: "+totalHeader);
					return i;
				} 
		    }
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return 0;
		}
		return 0;
	}
}
