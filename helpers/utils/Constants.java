package utils;

public class Constants {
	
	// logigearEmail
	
	public static final String email_USERNAME = "thuy.tran";
	public static final String email_PASSWORD = "";
	public static final String data_location = "";
	
	// Timeout values
	public static int TIMEOUTS = 60;
	public static int SHORT_TIMEOUT = 30;
	
	// Hub URL
	public static final String HUB_URL = "localhost:4444/wd/hub";
	
	// AUT URLs
	public static final String AGODA_URL = "https://www.agoda.com/";
	public static final String VIETJET_URL = "https://www.vietjetair.com/Sites/Web/en-US/Home";
	public static final String LG_MAIL_URL = "https://sgmail.logigear.com/";
	
	public static final String vietjetInfoFilePath = Utilities.getProjectPath() + "\\test_data\\vietjet\\%s";
	
	
	// Enum Browser
	public enum Browser {
		CHROME("Chrome"), 
		FIREFOX("Firefox"), 
		REMOTECHROME("RemoteChrome"), 
		REMOTEFIREFOX("RemoteFirefox");

		private String strBrowser = null;

		Browser(String str) {
			strBrowser = str;
		}

		public String getBrowserString() {
			return strBrowser;
		}

		public void setBrowserString(String str) {
			strBrowser = str;
		}
		
		public static Browser getBrowserName(String browserName) {
			switch (browserName) {
			case "Chrome":
				return Browser.CHROME;
			case "Firefox":
				return Browser.FIREFOX;
			case "RemoteChrome":
				return Browser.REMOTECHROME;
			case "RemoteFirefox":
				return Browser.REMOTEFIREFOX;
			default:
				System.out.println("Not supported!");
				return null;
			}
		}
	}
	
	
	/**
	 * @author Thuy Tran
	 * @description Enum for Traveler Type
	 */
	public enum TravelerType {
		FAMILY("Family travelers"), GROUP("Group travelers"), BUSINESS("Business travelers"), SOLO(
				"Solo traveler"), COUPLE("Couple/Pair");

		private String strTravelerType = null;

		TravelerType(String str) {
			strTravelerType = str;
		}

		public String getTravelerTypeString() {
			return strTravelerType;
		}

		public void setTravelerTypeString(String str) {
			strTravelerType = str;
		}

		public static TravelerType returnTravelerTypeUsingText(String travelerTypeName) {
			switch (travelerTypeName) {
			case "FAMILY":
				return TravelerType.FAMILY;
			case "GROUP":
				return TravelerType.GROUP;
			case "BUSINESS":
				return TravelerType.BUSINESS;
			case "SOLO":
				return TravelerType.SOLO;
			case "COUPLE":
				return TravelerType.COUPLE;
			default:
				System.out.println("Not existed!");
				return null;

			}
		}
	}

	/**
	 * @author Thuy Tran
	 * @description Enum for Occupancy Type
	 */
	public enum OccupancyType {
		room("Room"), adult("Adult"),children("Children");

		private String strOccupancyType = null;

		OccupancyType(String str) {
			strOccupancyType = str;
		}

		public String getTravelerTypeString() {
			return strOccupancyType;
		}

		public void setTravelerTypeString(String str) {
			strOccupancyType = str;
		}
		
		public static OccupancyType returnOccupancyTypeUsingText(String typeName) {
			switch (typeName) {
			case "room":
				return OccupancyType.room;
			case "adult":
				return OccupancyType.adult;
			case "children":
				return OccupancyType.children;

			default:
				System.out.println("Not existed!");
				return null;
	 					
	 			}
		}
	}
	
	/**
	 * @author Thuy Tran
	 * @description Enum for Filter options
	 */
	public enum FilterOptions {
		popular("Popular"), budget("Your budget"),stars("Stars"),location("Locations");

		private String strFilterOptions = null;

		FilterOptions(String str) {
			strFilterOptions = str;
		}

		public String getFilterOptionsString() {
			return strFilterOptions;
		}

		public void setFilterOptionsString(String str) {
			strFilterOptions = str;
		}
		
		@Override public String toString() { return strFilterOptions; }
	}
}
