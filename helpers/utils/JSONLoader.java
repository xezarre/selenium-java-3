package utils;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONLoader<T> {
	
	private final static String path = Utilities.getProjectPath() + "\\test_data\\%s\\%s";
	
	@SuppressWarnings("unchecked")
	public T loadJsonContent(String folder, String fileName, Class<?> c) 
	{
		Object obj = null;
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			obj = mapper.readValue(new File (String.format(path, folder, Utilities.validateJSONFileName(fileName))), c);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) obj;
	}
	
	/**
	 * Get json node using json pointer
	 * @author Hoa
	 * @return a JsonNode object - can convert value using asText() or asInt() method
	 * @param filePath - path to file in project folder - omit path to project folder
	 * @param jsonPointer - json pointer, e.g. "/aut_url/lg_mail_url" or "/destination"
	 */
	public static JsonNode getJsonNodeByJsonPointer(String filePath, String jsonPointer) 
	{		
		JsonNode node = null;
		try {
			node = new ObjectMapper().readTree(new File(Utilities.getProjectPath() + filePath));
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return node.at(jsonPointer);
	}

}
