package utils;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.imageio.ImageIO;
import org.openqa.selenium.support.Color;
import settings.oSettings;

public class Utilities {
	
	public static String getProjectPath() {
		return System.getProperty("user.dir");
	}
	
	public static String generateRandomString(int strLength) {
		String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String returnString = "";

		Random rand = new Random();

		char[] text = new char[strLength];

		for (int i = 0; i < strLength; i++) {
			text[i] = letters.charAt(rand.nextInt(letters.length()));
			returnString += text[i];
		}

		return returnString;
	}
	
	/**
	 * Capture screen
	 * @author Hoa
     * @param path path to file, use double slash instead of single slash
     * @param imgExt image extension. Recommend jpg
     * @param filename filename, without extension
     */
	public static void captureScreen(String path, String imgExt, String filename) {
		
		try {
			BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			ImageIO.write(image, imgExt, new File(path + "//" + filename +".jpg"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Validate file name.
	 * @author Vinh
	 * @param sFileName
	 * @return
	 */
	public static String validateJSONFileName(String sFileName)
	{
		if (null == sFileName.trim()) 
			return "";
		if (sFileName.trim().endsWith(".json")) 
			return sFileName;
		if (!sFileName.trim().contains(".")) 
			return sFileName + ".json";
		return "";
	}
	
	/**
	 * Convert color to Hexa value.
	 * @author Vinh
	 * @param colorString
	 * @return
	 */
	public static String getHexColor(String colorString) {
		return Color.fromString(colorString).asHex();
	}
	
	public static String getUrlOf(String application) {
		
		oSettings setting = oSettings.loadSettings();
		switch (application) {
		case "Agoda":
			return setting.getAgodaUrl();
		case "Vietjet":
			return setting.getVietjetUrl();
		case "LGMail":
			return setting.getLgmailUrl();
		default:
			System.out.println("AUT unsupported!");
			return null;
		}
	}
	
	/**
	 * return integer random
	 * @author Thuy Tran
	 * @param min
	 * @param max
	 * @return
	 */
	public static int randomInt(int min, int max) {
		
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
		
		return randomNum;
	}
	
	/**
	 * Check if a file is an image.
	 * @param file
	 * @return
	 */
	public static Boolean isImage(File file) {
		
		try {
		    Image image = ImageIO.read(file);
		    if (image == null) return false;
		} catch (IOException e) {
		   e.printStackTrace();
		}
		return true;
	}
}

