package driver;

import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;

import utils.Logger;

public class BaseDriver {
	
	protected WebDriver _webDriver;
	protected WebElement _webElement;
	protected Logger logger = new Logger();

	public BaseDriver(WebDriver webDriver) {
		this._webDriver = webDriver;
	}

	/**
	 * Get current page url
	 * @return
	 */
	public String getCurrentUrl() {
		logger.printMessage("Get Current Url ");
		String strURL = null;
		try {
			strURL = _webDriver.getCurrentUrl();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot get current URL due to '" + e.getMessage() + "'");
		}
		return strURL;
	}

	/**
	 * Get Page title
	 * @return
	 */
	public String getTitle() {
		String strTitle = null;
		try {
			strTitle = _webDriver.getTitle();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot get title due to '" + e.getMessage() + "'");
		}
		return strTitle;
	}

	/**
	 * Get page source
	 * @return
	 */
	public String getPageSource() {
		String strPageSource = null;
		try {
			strPageSource = _webDriver.getPageSource();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot get page source due to '" + e.getMessage() + "'");
		}
		return strPageSource;
	}

	/**
	 * Close current page
	 */
	public void close() {
		logger.printMessage("Close current page");
		try {
			_webDriver.close();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot find element due to '" + e.getMessage() + "'");
		}
	}

	/**
	 * Quit browser
	 */
	public void quit() {
		logger.printMessage("Quit browser");
		try {
			_webDriver.quit();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot quit due to '" + e.getMessage() + "'");
		}
	}

	/**
	 * Get windows handles
	 * @return
	 */
	public ArrayList<String> getWindowHandles() {
		try {
			return new ArrayList<String>(_webDriver.getWindowHandles());
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}

	/**
	 * Get windows handle
	 * @return
	 */
	public String getWindowHandle() {
		try {
			return _webDriver.getWindowHandle();
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}

	/**
	 * Switch handling
	 * @return
	 */
	public TargetLocator switchTo() {
		try {
			return _webDriver.switchTo();
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Switch to window
	 * @param windowHandle
	 */
	public void switchTo(String windowHandle) {
		_webDriver.switchTo().window(windowHandle);
	}
	
	/**
	 * @author: Thuy Tran
	 * @description: Switch window by title.
	 * @param title
	 */
	public void switchWindow(String title) {
		
	    String currentWindow = _webDriver.getWindowHandle();
	    Set<String> availableWindows = _webDriver.getWindowHandles();
	    
	    if (!availableWindows.isEmpty()) {
	    	for (String windowId : availableWindows) {
	    		if (_webDriver.switchTo().window(windowId).getTitle().equals(title)) {
	    		} else {
	    			_webDriver.switchTo().window(currentWindow);
	    		}
	    	}
	    }
	}
	
	/**
	 * @description: Switch iFrame by id name
	 * @author Thuy Tran
	 * @param iframeName
	 */
	public void switchFrame(String iframeName) {
	
		if (iframeName != "default") {
			_webDriver.switchTo().frame(iframeName);
		}
		else {
			_webDriver.switchTo().defaultContent();
		}
	}
	
	/**
	 * @description: Switch iFrame by index 
	 * @author: Thuy Tran
	 * @param index
	 */
	public void switchFrame(int index)
	{
		_webDriver.switchTo().frame(index);
	}
	
	/**
	 * @author: Thuy Tran
	 * @description: Switch iFrame by iframe 
	 */	
	public void switchParentFrame() {
		_webDriver.switchTo().parentFrame();
	}

	/**
	 * manage() method
	 * @return
	 */
	public Options manage() {
		try {
			return _webDriver.manage();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot navigate due to '" + e.getMessage() + "'");
			return null;
		}
	}

	/**
	 * Maximize browser windows
	 */
	public void maximize() {
		try {
			_webDriver.manage().window().maximize();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot maximize due to '" + e.getMessage() + "'");
		}
	}

	/**
	 * Navigate to URL
	 * @param url
	 */
	public void navigateToURL(String url) {
		try {
			this._webDriver.navigate().to(url);
		} catch (Exception e) {
			logger.printMessage("Error: Cannot navigate to " + url + "due to '" + e.getMessage() + "'");
		}
	}
	
	/**
	 * @author: Thuy Tran
	 * @description: Switch window
	 */
	public void closeWindow() 
	{
		_webDriver.close();
	}
	
	public void refresh() {
		try {
			this._webDriver.navigate().refresh();
		} catch (Exception e) {
			logger.printMessage("Error: Cannot refresh page due to '" + e.getMessage() + "'");
		}
	}
}
