package driver;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import utils.Constants;
import utils.Constants.Browser;

public class DriverManager {

	static WebDriver _webDriver;
	static Map<String, CustomWebDriver> driverController = new HashMap<String, CustomWebDriver>();
	
	public static void initBrowser(Browser enumBrowser) {
		try {
			DesiredCapabilities dc;
			Class<? extends WebDriver> driverClass = null;
			
			switch (enumBrowser) {
			
				case CHROME:
					driverClass = ChromeDriver.class;
					WebDriverManager.getInstance(driverClass).setup();
					_webDriver = driverClass.newInstance();
					break;
					
				case FIREFOX:
					driverClass = FirefoxDriver.class;
					WebDriverManager.getInstance(driverClass).setup();
					_webDriver = driverClass.newInstance();
					break;
					
				case REMOTECHROME:
					dc = DesiredCapabilities.chrome();
					dc.setBrowserName("chrome");
					_webDriver = new RemoteWebDriver(new URL(Constants.HUB_URL), dc);
					break;
					
				case REMOTEFIREFOX:
					dc = DesiredCapabilities.firefox();
					dc.setBrowserName("firefox");
					_webDriver = new RemoteWebDriver(new URL(Constants.HUB_URL), dc);
					break;
					
				default:
						System.out.println("Not supported yet!");
				}
			
			CustomWebDriver customWebDriver = new CustomWebDriver(_webDriver);
			DriverManager.setDriver(customWebDriver);
		}
		catch(Exception e)
		{
			System.out.println("Error: Cannot initiate driver due to '" + e + "'");
		}
	}
	
	public static void setDriver(CustomWebDriver customWebDriver) {
		driverController.put(String.valueOf(Thread.currentThread().getId()), customWebDriver);
	}
	
	public static CustomWebDriver getDriver() {
		return driverController.get(String.valueOf(Thread.currentThread().getId()));
	}
}
