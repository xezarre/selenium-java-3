package driver;

import org.openqa.selenium.WebDriver;

public class CustomWebDriver extends BaseDriver{

	public CustomWebDriver(WebDriver webDriver) {
		super(webDriver);
	}
	
	public WebDriver getWebDriver() {
		return this._webDriver;
	}
	
	
}
