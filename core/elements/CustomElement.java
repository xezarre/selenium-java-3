package elements;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Stopwatch;
import utils.Constants;

public class CustomElement extends BaseElement {

	public CustomElement(By by) {
		this.by = by;
	}

	public CustomElement(String Xpath) {
		this.by = By.xpath(Xpath);
	}
	
	public CustomElement(String dynamicXpath, String text) {
		this.by = By.xpath(String.format(dynamicXpath, text));
	}
	
	public CustomElement() {}

	public void set(boolean selected) {
		try {
			waitForClickable(by);
			if (_webElement.isSelected() != selected) {
				_webElement.click();
			}
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}
	
	/**
	 * Click element with default timeout.
	 */
	public void click() {
		click(Constants.SHORT_TIMEOUT);
	}
	
	/**
	 * Click element.
	 * @param timeOutInSeconds
	 */
	public void click(int timeOutInSeconds) {
		
		if (timeOutInSeconds < 0) {
			logger.printMessage("Time out cannot be less than 0");
			return;
		}
		Stopwatch sw = Stopwatch.createStarted();
		try {			
			waitForClickable(by,timeOutInSeconds);
			_webElement.click();
		} catch (StaleElementReferenceException staleElement) {
			
			if (sw.elapsed(TimeUnit.SECONDS) <= (long) timeOutInSeconds) {
				click(timeOutInSeconds - (int) sw.elapsed(TimeUnit.SECONDS));
			}
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}
	
	/**
	 * @author Vinh
	 * Click element then wait until page loaded completely
	 */
	public void clickAndWaitForPageLoaded() {
		click();
		waitForPageLoad();
	}
	
	public void clickAndWaitForElementsPresence(By by) {
		click();
		waitForElementsPresence(by);
	}

	public void moveMouse() {
		moveMouse(Constants.SHORT_TIMEOUT);
	}
	
	public void moveMouse(int timeOutInSeconds) {
		Stopwatch sw = Stopwatch.createStarted();
		try {
			waitForVisibility(timeOutInSeconds);
			Actions builder = new Actions(_customWebDriver.getWebDriver());
			Action actMoveMouse = builder.moveToElement(_webElement).build();
			actMoveMouse.perform();
		} catch (StaleElementReferenceException e) {
			if (sw.elapsed(TimeUnit.SECONDS) <= (long) timeOutInSeconds) {
				moveMouse(timeOutInSeconds - (int) sw.elapsed(TimeUnit.SECONDS));
			}
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}

	public void enter(String input) {
		try {
			if (!getAttribute("value").equals(input)) {
				_webElement.click();
				_webElement.clear();
				_webElement.sendKeys(input);
			}
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}
	
	public void type(String input) {
		
		waitForVisibility(1);
		_webElement.click();
		_webElement.clear();
		_webElement.sendKeys(input);
		_webElement.sendKeys(Keys.END); // Handle email suggestion element that blocks
	}
	
	/**
	 * @author Vinh
	 * Scroll element to the top.
	 */
	public void scrollToElement(String xpath) {
		((JavascriptExecutor) _customWebDriver.getWebDriver())
				.executeScript("arguments[0].scrollIntoView(true);", _customWebDriver.getWebDriver().findElement(By.xpath(xpath)));
	}
	
	/**
	 * Scroll the element into the middle of screen.
	 * @author Vinh
	 * @param xpath
	 */
	public void scrollElementIntoMiddle(String xpath) {
		String script = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		
		((JavascriptExecutor) _customWebDriver.getWebDriver())
				.executeScript(script, _customWebDriver.getWebDriver().findElement(By.xpath(xpath)));
	}
	
	/**
	 * @author vinh
	 * Wait for Page loaded completely
	 */
	public void waitForPageLoad() {
	    new WebDriverWait(_customWebDriver.getWebDriver(), 30).until((ExpectedCondition<Boolean>) wd ->
	            ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
	}
	
	/**
	 * Wait for Presence of Elements defined by By object
	 * @author Vinh
	 * @param by
	 */
	public void waitForElementsPresence(By by) {
		new WebDriverWait(_customWebDriver.getWebDriver(), 30).until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
	}

	/**
	 * @author Vinh
	 * @param xPath
	 * @param propertyName
	 * @return
	 */
	public String getCssValue(String xPath, String propertyName) {
		return _customWebDriver.getWebDriver().findElement(By.xpath(xPath)).getCssValue(propertyName);
	}

	@Override
	public List<WebElement> findElements(By by) {
		return _webElements;
	}
}
