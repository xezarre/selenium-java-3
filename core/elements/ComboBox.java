package elements;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ComboBox extends CustomElement {

	/**
	 * Combo box instructor.
	 * @param Xpath
	 */
	public ComboBox(String Xpath) {
		super(Xpath);
	}
	
	private static Select dropdown(WebElement element) {
		return new Select(element);
	}	
	
	/**
	 * Select value by visible text
	 * @author: Hien Khuu
	 * @param strItemText
	 */
	public void selectByVisibleText(String strItemText) {
		try {
			waitForClickable(by);			
			dropdown(_webElement).selectByVisibleText(strItemText);
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}

	/**
	 * Get all items in the specified 'Select' dropdown
	 * @return
	 */
	public String[] getAllItemsInDropdown() {
		try {
			waitForClickable(by);
			List<WebElement> listOptions = dropdown(_webElement).getOptions();
			String[] arrResult = new String[listOptions.size()];
			
			int i = 0;
			for (WebElement element : listOptions) {
				
				arrResult[i] = element.getText();
				i++;
			}
			return arrResult;
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}

	/**
	 * Get current selected value in 'Select' dropdown
	 * @return
	 */
	public String getSelected() {
		try {
			waitForClickable(by);
			return dropdown(_webElement).getFirstSelectedOption().getText();
		} catch (Exception e) {
			logger.printMessage("Cannot get current selected value of '" + by + "' due to '" + e.getMessage() + "'");
			return null;
		}
	}

}
