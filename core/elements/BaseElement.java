package elements;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Stopwatch;

import driver.CustomWebDriver;
import driver.DriverManager;
import utils.Constants;
import utils.Logger;

abstract class BaseElement implements WebElement {
	
	protected CustomWebDriver _customWebDriver = DriverManager.getDriver();
	protected WebElement _webElement;
	protected Logger logger = new Logger();
	protected By by;

	protected List<WebElement> _webElements;

	// INSTRUCTORS //
	protected BaseElement() {
		_webElement = null;
	}

	protected BaseElement(By by) {
		this.by = by;
	}

	public BaseElement(String strXpath) {
		by = By.xpath(strXpath);
	}

	protected BaseElement(WebElement webElement) {
		this._webElement = webElement;
	}
	
	
	// METHODS //
	
	public By getLocator() {
		return by;
	}
	
	/**
	 * Get Element attribute.
	 */
	public String getAttribute(String name) {
		try{
			waitForClickable(by);
			return _webElement.getAttribute(name);
		} catch(Exception e){
			logger.printMessage(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Get Element text
	 */
	public String getText() {
		try {
			waitForClickable(by);
			return _webElement.getText();
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Check if Element is selected
	 */
	public boolean isSelected() {
		try {			
			waitForClickable(by);
			return _webElement.isSelected();
		} catch (Exception e) {
			logger.printMessage(String.format("IsSelected: Has error with control '%s': %s", getLocator().toString(),
					e.getMessage()));
			return false;
		}
	}
	
	/**
	 * Check if Element is enabled
	 */
	public boolean isEnabled() {
		try {			
			waitForVisibility(5);
			return _webElement.isEnabled();
		} catch (Exception e) {
			logger.printMessage(String.format("IsEnabled: Has error with control '%s': %s", getLocator().toString(),
					e.getMessage()));
			return false;
		}
	}
	
	/**
	 * Check if Element is displayed
	 */
	public boolean isDisplayed() {
		return isDisplayed(0);
	}
	
	/**
	 * Check if Element is displayed with timeout value.
	 */
	public boolean isDisplayed(int timeOutInSeconds) {
		try {
			waitForVisibility(timeOutInSeconds);
			return _webElement.isDisplayed();
		} catch (NullPointerException e) {
			return false;
		} catch (Exception e) {
			logger.printMessage(String.format("IsDisplayed: Has error with control '%s': %s", getLocator().toString(),
					e.getMessage()));
			return false;
		}
	}
	
	/**
	 * @author Thuy
	 * @return
	 */
	public boolean isExisted() {
		if (_customWebDriver.getWebDriver().findElements(this.getLocator()).size() != 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Return TRUE if element is invisible. Otherwise, return FALSE
	 * @param timeOutInSeconds
	 * @return
	 */
	@SuppressWarnings("finally")
	public boolean isInvisible(int timeOutInSeconds) {
		boolean result = false;
		try {
			waitForInvisibility(timeOutInSeconds);
			if (!isDisplayed(0)) {
				result = true;
			}
		} catch (Exception e) {
			logger.printMessage(String.format("IsDisplayed: Has error with control '%s': %s", getLocator().toString(),
					e.getMessage()));
			result = false;
		} finally {
			return result;
		}
	}
	
	/**
	 * Wait for Element clickable.
	 * @param by
	 * @param timeOutInSeconds
	 */
	public void waitForClickable(By by, int timeOutInSeconds) {
		try {
			WebDriverWait wait = new WebDriverWait(_customWebDriver.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.and(ExpectedConditions.presenceOfElementLocated(by), ExpectedConditions.visibilityOfElementLocated(by)));
			_webElement = wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
		}
	}
	
	public void waitForClickable(int timeOut) {
		waitForClickable(this.by, timeOut);
	}

	public void waitForClickable() {
		waitForClickable(this.by, Constants.SHORT_TIMEOUT);
	}
	
	public void waitForClickable(By by) {
		waitForClickable(by, Constants.SHORT_TIMEOUT);
	}
	
	/**
	 * Wait for visibility
	 * @param timeOutInSeconds
	 */
	public void waitForVisibility(int timeOutInSeconds) {
		try {
			WebDriverWait wait = new WebDriverWait(_customWebDriver.getWebDriver(), timeOutInSeconds);
			_webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(getLocator()));
		} catch (Exception e) {
			//logger.printMessage(e.getMessage());
		}
	}
	
	/**
	 * Wait for the invisibility of element.
	 * @param timeOutInSeconds
	 */
	public void waitForInvisibility(int timeOutInSeconds) {
		logger.printMessage("Wait for element disappears");
		Stopwatch sw = Stopwatch.createStarted();
		WebDriverWait wait = new WebDriverWait(_customWebDriver.getWebDriver(), timeOutInSeconds);

		while (sw.elapsed(TimeUnit.SECONDS) <= (long) timeOutInSeconds) {
			try {
				boolean present = wait.ignoring(StaleElementReferenceException.class)
						.ignoring(NoSuchElementException.class)
						.until(ExpectedConditions.invisibilityOfElementLocated(by));
				if (present == true) {
					break;
				}
			} catch (NoSuchElementException e) {
				break;
			} catch (Exception e) {
				logger.printMessage(String.format("waitForInvisibility: Has error with control '%s': %s", getLocator().toString(),
				e.getMessage()));
			}
		}
	}
	
	/**
	 * Wait for elements clickable
	 * @param by
	 * @param timeOutInSeconds
	 * @return
	 */
	public List<WebElement> waitForElementsClickable(By by, int timeOutInSeconds) {
		try {
			WebDriverWait wait = new WebDriverWait(_customWebDriver.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
			_webElements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			return _webElements;
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}

	/**
	 * Wait for Elements clickable with default timeout
	 * @return
	 */
	public List<WebElement> waitForElementsClickable() {
		return waitForElementsClickable(by, Constants.SHORT_TIMEOUT);
	}

	/**
	 * Wait for Elements clickable with input timeout in seconds
	 * @param timeOutInSeconds
	 * @return
	 */
	public List<WebElement> waitForElementsClickable(int timeOutInSeconds) {
		return waitForElementsClickable(by, timeOutInSeconds);
	}
	
	public <X> X getScreenshotAs(OutputType<X> target) {
		throw new UnsupportedOperationException("Not support this function yet!");
	}
	
	public void submit() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	public void sendKeys(CharSequence... keysToSend) {
		throw new UnsupportedOperationException("Not supported this function anymore! Please use \"enter\" instead.");
	}

	
	public void clear() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	public String getTagName() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	
	public List<WebElement> findElements(By by, Integer timeOutInSeconds) {
		try {
			new WebDriverWait(_customWebDriver.getWebDriver(), timeOutInSeconds)
				.until(ExpectedConditions.and(ExpectedConditions.presenceOfAllElementsLocatedBy(by),
											  ExpectedConditions.visibilityOfAllElementsLocatedBy(by)));
			return _customWebDriver.getWebDriver().findElements(by);
		} catch (Exception e) {
			logger.printMessage(e.getMessage());
			return null;
		}
	}
	
	public List<WebElement> findElements(String xPath){
		return this.findElements(By.xpath(xPath), 5);
	}

	
	public WebElement findElement(By by) {
		throw new UnsupportedOperationException("Not supported this function anymore!");
	}

	public Point getLocation() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	public Dimension getSize() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	public Rectangle getRect() {
		throw new UnsupportedOperationException("Not support this function yet!");
	}

	/**
	 * @author Vinh
	 */
	public String getCssValue(String propertyName) {
		return _webElement.getCssValue(propertyName);
	}
}
